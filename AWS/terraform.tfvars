#region
region = "ap-south-1"
#default tags
tags = {
  "owner" = "changeMe"
}
#VPC
vpc_creation             = true
vpc_name                 = "changeme"
vpc_cidr_block           = "10.200.0.0/16"
vpc_enable_dns_hostnames = true
vpc_enable_dns_support   = true
vpc_tags = {
  "Managed by" = "Terraform"
}
#central vpc id if not creating vpc use this. if creating vpc then it must be black "". IF YOU ARE NOT CREATING VPC THEN ASSIGN SOME VALUE HERE
vpc_id = "" #"vpc-1234567"
#central availbilty zone
availability_zone = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]
#private subnets
create_private_subnets = true
private_subnets_name   = "changeme"
private_subnets        = ["10.200.1.0/24", "10.200.2.0/24", "10.200.3.0/24"] #if you are not creating private subnets then empty the list to reduce conflicts "[]"
private_subnets_tags = {
  "Managed by" = "Terraform"
}
#NAT
nat_create           = true
nat_name             = "changeme"
nat_public_subnet_id = "" #"changeme-1234567890" ##if creating public subnet then it must be blank. IF YOU ARE NOT CREATING PUBLIC SUBNET THEN ASSIGN SOME VALUE HERE
nat_tags = {
  "Managed by" = "Terraform"
}
#public subnets
create_public_subnets = true
igw_create            = true
public_subnet_name    = "changeme"
public_subnets        = ["10.200.101.0/24", "10.200.102.0/24", "10.200.103.0/24"]
public_subnets_tags = {
  "Managed by" = "Terraform"
}
#database subnets
create_database_subnets = false
database_subnets_name   = "changeme"
database_subnets        = [] #["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"] #if you are not creating database subnets then empty the list to reduce conflicts "[]"
database_subnets_tags = {
  "Managed by" = "Terraform"
}
#eks subnets
create_eks_subnets = true
eks_subnets_name   = "changeme"
eks_subnets        = ["10.200.7.0/24", "10.200.8.0/24", "10.200.9.0/24"] #if you are not creating eks subnets then empty the list to reduce conflicts "[]"
eks_subnets_tags = {
  "Managed by" = "Terraform"
}
#flow logs cloudwatch
create_flow_logs_cloudwatch = false
flow_logs_cloudwatch_name   = "changeme"
#flow logs s3
create_flow_logs_s3 = false
flow_logs_s3_name   = "changeme"


# ------ OpenSearch -----------------------------------------------------------

opensearch_cluster_config = [
  {
    domain                   = "newdomain"
    elasticsearch_version    = "7.10"    
    # advanced_options         = ""

    instance_count           = 2
    instance_type            = "t3.small.elasticsearch"
    zone_awareness_enabled   = true
    dedicated_master_enabled = false
    dedicated_master_count   = 2
    dedicated_master_type    = "t2.small.elasticsearch"
    warm_instance_enabled    = false 
    warm_instance_count      = 2
    warm_instance_type       = "ultrawarm1.large.elasticsearch"
 
    ebs_volume_size          = 10
    ebs_volume_type          = "gp2"
    ebs_iops                 = 0

    advanced_security_options       = true
    internal_user_database_enabled  = true
    master_user_name                = "esadmin"
    master_user_password            = "ESadmin@5678910"

    custom_endpoint_enabled         = false 
    custom_endpoint                 = ""  ## optinal
    custom_endpoint_certificate_arn = ""  ## optinal
 
    encrypt_at_rest_enabled         = true
    encrypt_at_rest_kms_key_id      = ""  ## optinal
    node_to_node_encryption_enabled = true
    automated_snapshot_start_hour   = 23 
  }
]

#------- secret-smanager  ---------------------------------------------------

secret_names=["prod/prod-notification","prod/prod-icp","prod/prod-admin","prod/prod-reports","prod/prod-backend"]
secret_files=["./files/prod-notification.json","./files/prod-icp.json","./files/prod-admin.json","./files/prod-reports.json","./files/prod-backend.json"]

#------- Lambda  ---------------------------------------------------
 
iam_for_lambda_file         = "./files/iam_for_lambda.json"
iam_policy_for_lambda_file  = "./files/iam_policy_for_lambda.json"
lambda-slack-zip            = "./files/lambda-slack.zip"

ec2_instances = [
  {
    ami           = "ami-04bad3c587fe60d89"
    instance_type = "t2.micro"
    volume_type   = "gp3"
    volume_size   = 8
    delete_on_termination_rblk   = false
    ebs_block_devices = [
      {
      delete_on_termination_ebsblk = false
      device_name   = "/dev/sdb"
      volume_type   = "gp3"
      volume_size   = 20
      encrypted     = true 
      },
      {
      delete_on_termination_ebsblk = false
      device_name   = "/dev/sdc"
      volume_type   = "gp3"
      volume_size   = 25
      encrypted     = true 
      }
    ] 
    key_name      = "demotfkey"
    tags          = ["prod-jenkins", "prod", "myproject","jenkins-app","terraform"]
    subnet_id   = "subnet-3c78lu01"
  },
  {
    ami           = "ami-04bad3c587fe60d89"
    instance_type = "t2.micro"
    volume_type   = "gp3"
    volume_size   = 8
    delete_on_termination_rblk   = false
    ebs_block_devices = [ 
      {
      delete_on_termination_ebsblk = false
      device_name   = "/dev/sdb"
      volume_type   = "gp3"
      volume_size   = 20
      encrypted     = true         
      }
    ]
    key_name      = "demotfkey"
    tags          = ["monitor-vpn", "prod", "myproject","monitoring-app","terraform"]
    subnet_id     = "subnet-8c68jk01"
  }
]

# ------S3 bucket -----------------------------------------------------------------------

bucket_config = [
  {
    bucket_name = "my-demo-bucket-tf-00"
    enable_versioning = true
    enable_server_side_encryption = true
    enable_lifecycle_rule = true
    days = 30
    storage_class = "STANDARD_IA"
    noncurrent_days = 365
    noncurrent_storage_class = "GLACIER"
  },
  {
    bucket_name = "my-demo-bucket-tf-01"
    enable_versioning = true
    enable_server_side_encryption = true
    enable_lifecycle_rule = true
    days = 30
    storage_class = "STANDARD_IA"
    noncurrent_days = 365
    noncurrent_storage_class = "GLACIER"
  },
  {
    bucket_name = "my-demo-bucket-tf-02"
    enable_versioning = true
    enable_server_side_encryption = true
    enable_lifecycle_rule = true
    days = 30
    storage_class = "STANDARD_IA"
    noncurrent_days = 365
    noncurrent_storage_class = "GLACIER"
  }
]

# ------- SQS ----------------------------------------------------------------------

sqs_queue_config = [
  {
    sqs_queue_name = "my-demo-sqs-tf-00"
    max_message_size          = 2048
    message_retention_seconds = 86400
    receive_wait_time_seconds = 10
    kms_data_key_reuse_period_seconds = 300
    delay_seconds             = 90
  },
  {
    sqs_queue_name = "my-demo-sqs-tf-01"
    max_message_size          = 2048
    message_retention_seconds = 86400
    receive_wait_time_seconds = 10
    kms_data_key_reuse_period_seconds = 300
    delay_seconds             = 90
  },
]

# -------KMS----------------------------------------------------------------------

create_kms_keys = true
key_aliases = [
  {
    key_alias        = "alias/eks_tf_v1"
    key_description  = "KMS key for eks"
    key_spec         = "SYMMETRIC_DEFAULT"
    enabled          = true
    rotation_enabled = true
    deletion_window  = 7
    kms_policy_enabled = true 
  },
  {
    key_alias        = "alias/db"
    key_description  = "KMS key for db"
    key_spec         = "SYMMETRIC_DEFAULT"
    enabled          = true
    rotation_enabled = true
    deletion_window  = 7
    kms_policy_enabled = false
  },
  {
    key_alias = "alias/sqs"
    key_description = "Example KMS SQS"
    key_spec= "SYMMETRIC_DEFAULT"
    enabled= true
    rotation_enabled= true
    deletion_window= 7 
    kms_policy_enabled = false

  }  
]

# -----------------------------------------------------------------------------

kms_policy = "files/eks-kms-policy.json"

#####EKS
create_eks_launch_template                = true
eks_launch_template_name                  = "changeme-lt"
eks_launch_template_instance_type         = "t3.medium"
eks_launch_template_volume_size           = 30
eks_launch_template_delete_on_termination = true
eks_launch_template_iops                  = 3000
eks_launch_template_key_name              = "abhishek-eks-key"
eks_launch_template_volume_type           = "gp3"
#EKS cluster role
eks_role_creation = true
eks_role_filename = "files/eks_cluster_policy.json"
eks_role_name     = "changeme-cluster-policy"
#EKS nodegroup role 
eks_nodegroup_role_creation = true
eks_nodegroup_role_filename = "files/eks_nodegroup_policy.json"
eks_nodegroup_role_name     = "changeme-node-group-policy"
#cluster
eks_cluster_creation           = true
eks_cluster_name               = "changeme"
eks_role_arn                   = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy" #if value is provided it will be reflected, if not it will take from module
eks_version                    = "1.24"
eks_subnet_ids                 = [] #["sb-12345634789", "sb-123456323789", "sb-12345678912"] #if value is provided it will be reflected, if not it will take from module
eks_endpoint_private_access    = true
eks_endpoint_public_access     = false
eks_kms_key_arn                = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
eks_resources_to_encrypt       = ["secrets"]
eks_cluster_log_retention_days = 7
eks_enabled_cluster_log_types  = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
create_eks_vpc_cni_addon       = false
create_eks_coredns_addon       = false
create_eks_kubeproxy_addon     = false
create_eks_ebscsi_addon        = false
eks_cluster_tags = {
  "Name" = "projectname"
}
#eks nodegroup
create_eks_node_group            = true
eks_nodegroup_role_arn           = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
eks_nodegroup_launch_template_id = ""
eks_nodegroup_config = [
  {
    capacity_type        = "SPOT"
    desired_size         = 1
    eks_nodegroup_tags   = { "Name" = "changeme" }
    force_update_version = false
    max_size             = 1
    max_unavailable      = 1
    min_size             = 1
    name                 = "changeme-ng-1"
    tags = {
      "Abhishek" = "2"
  } },
  {
    capacity_type        = "ON_DEMAND"
    desired_size         = 1
    eks_nodegroup_tags   = { "Name" = "changeme-2" }
    force_update_version = false
    max_size             = 1
    max_unavailable      = 1
    min_size             = 1
    name                 = "changeme-ng-2"
    tags                 = { "Abhishek" = "3" }
  }
]

#Database
#---
create_rds_sg = false
rds_sg_name   = "changeMe-rds-sg"
rds_sg_ingress_rules = [{
  cidr_blocks = ["10.0.4.0/24"]
  description = "this is subnet for db"
  from_port   = 3306
  protocol    = "tcp"
  to_port     = 3306
}]
rds_sg_egress_rules = [{
  cidr_blocks = ["0.0.0.0/0"]
  description = "internet connectivity"
  from_port   = 0
  protocol    = "-1"
  to_port     = 0
}]
rds_security_group_tags = {
  "Name" = "changeMe-tags-for-sg"
}
#subnet group
create_db_subnet_group      = false
db_subnet_group_name        = "changeMe-subnet-group"
db_subnet_group_description = "this is managed by terraform"
db_subnet_group_subnet_ids  = []
db_subnet_group_tags = {
  "Name" = "abhishek subnet grp"
}
#option group
create_db_option_group               = false
db_option_group_name                 = "changeMe-option-group"
db_option_group_description          = "this is changeMe's option group"
db_option_group_engine_name          = "mysql"
db_option_group_major_engine_version = "5.7"
db_option_group_options = [{
  option_name = "changeMe-group"
  option_settings = [{
    name  = "TIME_ZONE"
    value = "UTC"
  }]
  }
]
db_option_group_tags = {
  "Name" = "AOG"
}
#parameter group
create_parameter_group    = false
db_parameter_group_name   = "changeMe-pg"
db_parameter_group_family = "mysql"
db_parameter_group_parameter = {
  parameter1 = {
    name  = "log_connections"
    value = "1"
  }
  parameter2 = {
    name  = "log_connections"
    value = "2"
  }
}
db_parameter_group_tags = {
  "Name" = "PG"
}
#single db instance
db_instance_create_single_db_instance             = false
db_instance_allocated_storage                     = 50
db_instance_max_allocated_storage                 = 60
db_instance_storage_type                          = "gp3"
db_instance_engine                                = "mysql"
db_instance_engine_version                        = 5.7
db_instance_instance_class                        = "db.t3.medium"
db_instance_identifier                            = "changeMe"
db_instance_username                              = "root"
db_instance_password                              = "tooetootiifeifef"
db_instance_vpc_security_group_ids                = ["sg-22462486249"]
db_instance_maintenance_window                    = "Tue:00:00-Tue:03:00"
db_instance_backup_retention_period               = 1
db_instance_skip_final_snapshot                   = true
db_instance_allow_major_version_upgrade           = false
db_instance_auto_minor_version_upgrade            = false
db_instance_apply_immediately                     = false
db_instance_backup_window                         = "09:46-10:16"
db_instance_publicly_accessible                   = false
db_instance_db_name                               = "changeMe"
db_instance_db_subnet_group_name                  = "changeMe-subnet-grp"
db_instance_port                                  = 3306
db_instance_multi_az                              = false
db_instance_copy_tags_to_snapshot                 = true
db_instance_delete_automated_backups              = true
db_instance_parameter_group_name                  = "changeMe-para-grp"
db_instance_option_group_name                     = "changeMe-option-grp"
db_instance_kms_key_id                            = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
db_instance_rds_monitoring_role_name              = "changeMe-role"
db_instance_deletion_protection                   = false
db_instance_monitoring_interval                   = 0 #make this 0 when not creating rds
db_instance_monitoring_role_arn                   = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
db_instance_performance_insights_enabled          = true
db_instance_performance_insights_kms_key_id       = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
db_instance_performance_insights_retention_period = 7
db_instance_storage_encrypted                     = true
db_instance_rds_monitoring_filename               = "files/rds_monitoring_role.json"
db_instance_tags = {
  "Name" = "changeMe"
}
#replica
replica_db_create                                = false
replica_db_count                                 = 1
replica_db_allocated_storage                     = 50
replica_db_max_allocated_storage                 = 60
replica_db_storage_type                          = "gp3"
replica_db_instance_class                        = "db.t3.medium"
replica_db_identifier                            = "changeMe-rep"
replica_db_vpc_security_group_ids                = ["sg-22462486249"]
replica_db_maintenance_window                    = "Tue:00:00-Tue:03:00"
replica_db_backup_retention_period               = 1
replica_db_skip_final_snapshot                   = true
replica_db_allow_major_version_upgrade           = false
replica_db_auto_minor_version_upgrade            = false
replica_db_apply_immediately                     = false
replica_db_backup_window                         = "09:46-10:16"
replica_db_publicly_accessible                   = false
replica_db_port                                  = 3306
replica_db_multi_az                              = false
replica_db_copy_tags_to_snapshot                 = true
replica_db_delete_automated_backups              = true
replica_db_deletion_protection                   = false
replica_db_performance_insights_enabled          = true
replica_db_monitoring_interval                   = 0 #make this 0 when not creating rds replica
replica_db_monitoring_role_arn                   = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
replica_db_performance_insights_kms_key_id       = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
replica_db_performance_insights_retention_period = 7
replica_db_rds_monitoring_role_name              = "changeMe-role"
replica_db_rds_monitoring_filename               = "files/rds_replica_monitoring_role.json"
replica_db_storage_encrypted                     = true
replica_db_tags = {
  "Name" = "abhishek-replica"
}
#---
#security group for rds
# create_rds_sg = true
# rds_sg_name   = "changeme-rds-sg"
# rds_sg_ingress_rules = [{
#   cidr_blocks = ["10.0.4.0/24"]
#   description = "this is subnet for db"
#   from_port   = 3306
#   protocol    = "tcp"
#   to_port     = 3306
# }]
# rds_sg_egress_rules = [{
#   cidr_blocks = ["0.0.0.0/0"]
#   description = "internet connectivity"
#   from_port   = 0
#   protocol    = "-1"
#   to_port     = 0
# }]
# rds_security_group_tags = {
#   "Name" = "changeme-tags-for-sg"
# }
# #subnet group
# create_db_subnet_group      = true
# db_subnet_group_name        = "changeme-subnet-group"
# db_subnet_group_description = "this is managed by terraform"
# db_subnet_group_subnet_ids  = []
# db_subnet_group_tags = {
#   "Name" = "abhishek subnet grp"
# }
# #option group
# create_db_option_group               = true
# db_option_group_name                 = "changeme-option-group"
# db_option_group_description          = "this is changeme's option group"
# db_option_group_engine_name          = "mysql"
# db_option_group_major_engine_version = "5.7"
# db_option_group_options = [{
#   option_name = "changeme-group"
#   option_settings = [{
#     name  = "TIME_ZONE"
#     value = "UTC"
#   }]
#   }
# ]
# db_option_group_tags = {
#   "Name" = "AOG"
# }
# #parameter group
# create_parameter_group    = true
# db_parameter_group_name   = "changeme-pg"
# db_parameter_group_family = "mysql5.7"
# db_parameter_group_parameter = {
#   parameter1 = {
#     name  = "log_connections"
#     value = "1"
#   }
# }
# db_parameter_group_tags = {
#   "Name" = "PG"
# }
# #single db instance
# db_instance_create_single_db_instance             = true
# db_instance_allocated_storage                     = 50
# db_instance_max_allocated_storage                 = 60
# db_instance_storage_type                          = "gp3"
# db_instance_engine                                = "mysql"
# db_instance_engine_version                        = 5.7
# db_instance_instance_class                        = "db.t3.medium"
# db_instance_identifier                            = "changeme"
# db_instance_username                              = "root"
# db_instance_password                              = "tooetootiifeifef"
# db_instance_vpc_security_group_ids                = [] #["sg-22462486249"]
# db_instance_maintenance_window                    = "Tue:00:00-Tue:03:00"
# db_instance_backup_retention_period               = 1
# db_instance_skip_final_snapshot                   = true
# db_instance_allow_major_version_upgrade           = false
# db_instance_auto_minor_version_upgrade            = false
# db_instance_apply_immediately                     = false
# db_instance_backup_window                         = "09:46-10:16"
# db_instance_publicly_accessible                   = false
# db_instance_db_name                               = "changeme"
# db_instance_db_subnet_group_name                  = ""# "changeme-subnet-grp"
# db_instance_port                                  = 3306
# db_instance_multi_az                              = false
# db_instance_copy_tags_to_snapshot                 = true
# db_instance_delete_automated_backups              = true
# db_instance_parameter_group_name                  = "" #"changeme-para-grp"
# db_instance_option_group_name                     = "" #"changeme-option-grp"
# db_instance_kms_key_id                            = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
# db_instance_rds_monitoring_role_name              = "changeme-role"
# db_instance_deletion_protection                   = false
# db_instance_monitoring_interval                   = 7 #make this 0 when not creating rds
# db_instance_monitoring_role_arn                   = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
# db_instance_performance_insights_enabled          = true
# db_instance_performance_insights_kms_key_id       = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
# db_instance_performance_insights_retention_period = 7
# db_instance_storage_encrypted                     = true
# db_instance_rds_monitoring_filename               = "files/rds_monitoring_role.json"
# db_instance_tags = {
#   "Name" = "changeme"
# }
# #replica
# replica_db_create                                = true
# replica_db_count                                 = 1
# replica_db_allocated_storage                     = 50
# replica_db_max_allocated_storage                 = 60
# replica_db_storage_type                          = "gp3"
# replica_db_instance_class                        = "db.t3.medium"
# replica_db_identifier                            = "changeme-rep"
# replica_db_vpc_security_group_ids                = [] #["sg-22462486249"]
# replica_db_maintenance_window                    = "Tue:00:00-Tue:03:00"
# replica_db_backup_retention_period               = 1
# replica_db_skip_final_snapshot                   = true
# replica_db_allow_major_version_upgrade           = false
# replica_db_auto_minor_version_upgrade            = false
# replica_db_apply_immediately                     = false
# replica_db_backup_window                         = "09:46-10:16"
# replica_db_publicly_accessible                   = false
# replica_db_port                                  = 3306
# replica_db_multi_az                              = false
# replica_db_copy_tags_to_snapshot                 = true
# replica_db_delete_automated_backups              = true
# replica_db_deletion_protection                   = false
# replica_db_performance_insights_enabled          = true
# replica_db_monitoring_interval                   = 7 #make this 0 when not creating rds replica
# replica_db_monitoring_role_arn                   = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
# replica_db_performance_insights_kms_key_id       = "" #"arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
# replica_db_performance_insights_retention_period = 7
# replica_db_rds_monitoring_role_name              = "changeme-role"
# replica_db_rds_monitoring_filename               = "files/rds_replica_monitoring_role.json"
# replica_db_storage_encrypted                     = true
# replica_db_tags = {
#   "Name" = "abhishek-replica"
# }
#create vpc and subnets first and then create sg and then rds
#delete sg
#kms for rds is on 1 index so creating eks kms key
