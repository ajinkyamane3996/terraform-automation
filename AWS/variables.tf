# profile
variable "profile" {
  default = "ajinkya-tf"
  type    = string
}

#Region
variable "region" {
  type = string
}

#ALL DEFAULT TAGS

variable "tags" {
  type = map(string)
}

#VPC
variable "vpc_creation" {
  type = bool
}

variable "vpc_name" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "vpc_enable_dns_hostnames" {
  type = bool
}

variable "vpc_enable_dns_support" {
  type = bool
}

variable "vpc_tags" {
  type = map(string)
}

#central vpc id
variable "vpc_id" {
  type = string
}

#central availablity zone
variable "availability_zone" {
  type = list(string)
}

#private subnets
variable "create_private_subnets" {
  type = bool
}

variable "private_subnets_name" {
  type = string
}

variable "private_subnets" {
  type = list(string)
}

variable "private_subnets_tags" {
  type = map(string)
}

#NAT
variable "nat_create" {
  type = bool
}

variable "nat_name" {
  type = string
}

variable "nat_public_subnet_id" {
  type = string
}

variable "nat_tags" {
  type = map(string)
}

#public subnets
variable "create_public_subnets" {
  type = bool
}

variable "igw_create" {
  type = bool
}

variable "public_subnet_name" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

variable "public_subnets_tags" {
  type = map(string)
}

#database subnets
variable "create_database_subnets" {
  type = bool
}

variable "database_subnets_name" {
  type = string
}

variable "database_subnets" {
  type = list(string)
}

variable "database_subnets_tags" {
  type = map(string)
}

#EKS subnets
variable "create_eks_subnets" {
  type = bool
}

variable "eks_subnets_name" {
  type = string
}

variable "eks_subnets" {
  type = list(string)
}

variable "eks_subnets_tags" {
  type = map(string)
}

# flow logs cloudwatch

variable "create_flow_logs_cloudwatch" {
  type = bool
}

variable "flow_logs_cloudwatch_name" {
  type = string
}


#flow logs S3
variable "create_flow_logs_s3" {
  type = bool
}

variable "flow_logs_s3_name" {
  type = string
}


# variable alias {
#   type    = string
# }

# variable "kms_key_arn" { 
#   type = list(string)  
# } 

# variable "kms_key_ids" { 
#   type = list(string)  

# } 

#------- ec2 ---------------------------------------------#

variable "userdata_file" {
  type = string
  default = ""
}

variable "public-instance-sg" {
  description = "A list of security group IDs to associate with"
  type        = string
  default     = null
}

variable "sg-name" {
 type = string
 default = "pub-sg-ssh"
 description = "Name of the SG"
}

variable "ec2_instances" {
  type = list(object({
    ami           = string
    instance_type = string
    volume_type   = string
    volume_size   = number
    delete_on_termination_rblk = bool
    ebs_block_devices = list(object({
      delete_on_termination_ebsblk = bool
      device_name                  = string
      volume_size                  = number
      volume_type                  = string
      encrypted                    = bool
    }))
    # delete_on_termination_ebsblk = bool
    # device_name   = string
    # volume_type   = string
    # volume_size   = number
    # encrypted     = bool

    key_name      = string
    tags          = list(string)
    subnet_id = string
   }))
  default = [ ]
}

#------- KMS key ------------------------------------------#

variable "create_kms_keys" {
  type = bool
}

variable "key_aliases" {
  type = list(object({
    key_alias        = string
    key_description  = string
    key_spec         = string
    enabled          = bool
    rotation_enabled = bool
    deletion_window  = number
    kms_policy_enabled = bool
  }))
  default = []
}

variable "kms_policy" {
  type = string
}

#------- S3 bucket  ------------------------------------------#

variable "bucket_config" {
  type = list(object({
    bucket_name = string
    enable_versioning= bool
    enable_server_side_encryption= bool
    enable_lifecycle_rule= bool
    days = number
    storage_class= string
    noncurrent_days = number
    noncurrent_storage_class= string
  }))
  default = []
}

#------- SQS bucket  ------------------------------------------#

variable "sqs_queue_config" {
  type = list(object({
    sqs_queue_name = string
    max_message_size = number
    message_retention_seconds= number
    receive_wait_time_seconds= number
    delay_seconds= number
    kms_data_key_reuse_period_seconds= number
    # tags= []
  }))
  default = [ ]
}

#----------------------------------------------------------------#


####EKS
#iam role for cluster

variable "eks_role_creation" {
  type        = bool
  description = "true will create the role and false will not. can be also passed with tfvars"
}

variable "eks_role_name" {
  type        = string
  description = "role name for cluster if creating"
}

variable "eks_role_filename" {
  type        = string
  description = "file name in which policy for iam role is defined"
}


#iam role for nodegroups

variable "eks_nodegroup_role_creation" {
  type        = bool
  description = "true will create the role and false will not. can be also passed with tfvars"
}

variable "eks_nodegroup_role_name" {
  type        = string
  description = "role name for cluster if creating"
}

variable "eks_nodegroup_role_filename" {
  type        = string
  description = "file name in which policy for iam role is defined"
}

#eks launch template
variable "create_eks_launch_template" {
  type = bool
}

variable "eks_launch_template_name" {
  type = string
}

variable "eks_launch_template_instance_type" {
  type = string
}

variable "eks_launch_template_volume_size" {
  type = number
}


variable "eks_launch_template_iops" {
  type = number
}

variable "eks_launch_template_volume_type" {
  type = string
}

variable "eks_launch_template_delete_on_termination" {
  type = bool
}

variable "eks_launch_template_encrypted" {
  type    = bool
  default = true
}

variable "eks_launch_template_key_name" {
  type = string
}

#Cluster
variable "eks_cluster_creation" {
  type = bool
}

variable "eks_role_arn" {
  type = string
}

variable "eks_cluster_name" {
  type = string
}

variable "eks_version" {
  type = string
}

# variable "eks_cluster_endpoint" {
#   type = string
# }

# variable "eks_certificate_authority" {
#   type = string
# }


# variable "eks_security_group_ids" {
#   type = list(string)
# }

variable "eks_subnet_ids" {
  type = list(string)
}

variable "eks_endpoint_private_access" {
  type = bool
}

variable "eks_endpoint_public_access" {
  type = bool
}

variable "eks_kms_key_arn" {
  type = string
}

variable "eks_cluster_log_retention_days" {
  type = number
}

variable "eks_enabled_cluster_log_types" {
  type = list(string)
}

variable "eks_resources_to_encrypt" {
  type = list(string)
}
variable "eks_cluster_tags" {
  type = map(string)
}

#EKS Addons
variable "create_eks_vpc_cni_addon" {
  type = bool
}

variable "create_eks_coredns_addon" {
  type = bool
}

variable "create_eks_kubeproxy_addon" {
  type = bool
}

variable "create_eks_ebscsi_addon" {
  type = bool
}


#eks nodegroup


variable "create_eks_node_group" {
  type = bool
}

variable "eks_nodegroup_config" {
  type = list(object({
    name          = string
    capacity_type = string
    #subnet_ids           = list(string)
    force_update_version = bool
    desired_size         = number
    min_size             = number
    max_size             = number
    max_unavailable      = number
    eks_nodegroup_tags   = map(string)
    tags                 = map(string)
  }))
}


variable "eks_nodegroup_launch_template_id" {
  type = string
}


variable "eks_nodegroup_role_arn" {
  type = string
}

#database

#security groups
variable "create_rds_sg" {
  type = bool
}

variable "rds_sg_name" {
  type = string
}

variable "rds_sg_ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of ingress rules for the security group"
  #default     = []
}


variable "rds_sg_egress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  description = "List of egress rules for the security group"
  #default     = []
}


variable "rds_security_group_tags" {
  type = map(string)
}

#subnet group

variable "create_db_subnet_group" {
  type = bool
}

variable "db_subnet_group_name" {
  type = string
}

variable "db_subnet_group_description" {
  type = string
}

variable "db_subnet_group_subnet_ids" {
  type = list(string)
}

variable "db_subnet_group_tags" {
  type = map(string)
}


#option group

variable "create_db_option_group" {
  type = bool
}

variable "db_option_group_name" {
  type = string
}

variable "db_option_group_engine_name" {
  type = string
}

variable "db_option_group_major_engine_version" {
  type = string
}

variable "db_option_group_description" {
  type = string
}

variable "db_option_group_options" {
  description = "A list of option settings to apply to the option group."
  type = list(object({
    option_name = string
    option_settings = list(object({
      name  = string
      value = string
    }))
  }))
}

variable "db_option_group_tags" {
  type = map(string)
}

#parameter group

variable "create_parameter_group" {
  type = bool
}

variable "db_parameter_group_name" {
  type = string
}

variable "db_parameter_group_family" {
  type = string
}

variable "db_parameter_group_parameter" {
  type = map(object({
    name  = string
    value = string
  }))

}

variable "db_parameter_group_tags" {
  type = map(string)
}

#single db instance
variable "db_instance_create_single_db_instance" {
  type = bool
}

variable "db_instance_allocated_storage" {
  type = number
}
variable "db_instance_max_allocated_storage" {
  type = number
}
variable "db_instance_storage_type" {
  type = string
}
variable "db_instance_engine" {
  type = string
}
variable "db_instance_engine_version" {
  type = number
}
variable "db_instance_instance_class" {
  type = string
}
variable "db_instance_identifier" {
  type = string
}

variable "db_instance_username" {
  type = string
}
variable "db_instance_password" {
  type = string
}
variable "db_instance_vpc_security_group_ids" {
  type = list(string)
}
variable "db_instance_maintenance_window" {
  type = string
}
variable "db_instance_backup_retention_period" {
  type = string
}
variable "db_instance_skip_final_snapshot" {
  type = bool
}
variable "db_instance_allow_major_version_upgrade" {
  type = bool
}
variable "db_instance_auto_minor_version_upgrade" {
  type = bool
}
variable "db_instance_apply_immediately" {
  type = bool
}
variable "db_instance_backup_window" {
  type = string
}

variable "db_instance_db_name" {
  type = string
}
variable "db_instance_db_subnet_group_name" {
  type = string
}
variable "db_instance_kms_key_id" {
  type = string
}
variable "db_instance_port" {
  type = string
}
variable "db_instance_multi_az" {
  type = bool
}
variable "db_instance_publicly_accessible" {
  type = bool
}

variable "db_instance_copy_tags_to_snapshot" {
  type = bool
}

variable "db_instance_delete_automated_backups" {
  type = bool
}
variable "db_instance_deletion_protection" {
  type = bool
}

variable "db_instance_monitoring_interval" {
  type = number
}

variable "db_instance_monitoring_role_arn" {
  type = string
}

variable "db_instance_performance_insights_enabled" {
  type = bool
}

variable "db_instance_performance_insights_kms_key_id" {
  type = string
}

variable "db_instance_performance_insights_retention_period" {
  type = number
}

variable "db_instance_storage_encrypted" {
  type = bool
}

#role
variable "db_instance_rds_monitoring_role_name" {
  type = string
}

variable "db_instance_rds_monitoring_filename" {
  type = string
}

variable "db_instance_parameter_group_name" {
  type = string
}

variable "db_instance_option_group_name" {
  type = string
}


variable "db_instance_tags" {
  type = map(string)
}



##replica
variable "replica_db_create" {
  type = bool
}

variable "replica_db_count" {
  type = number
}

variable "replica_db_replicate_source_db" {
  default = "abhishek"
  type    = string
}

variable "replica_db_allocated_storage" {
  type = number
}
variable "replica_db_max_allocated_storage" {
  type = number
}
variable "replica_db_storage_type" {
  type = string
}
# variable "replica_db_engine" {
#   type = string
# }
# variable "replica_db_engine_version" {
#   type = number
# }
variable "replica_db_instance_class" {
  type = string
}
variable "replica_db_identifier" {
  type = string
}

# variable "replica_db_username" {
#   type = string
# }
# variable "replica_db_password" {
#   type = string
# }
variable "replica_db_vpc_security_group_ids" {
  type = list(string)
}
variable "replica_db_maintenance_window" {
  type = string
}
variable "replica_db_backup_retention_period" {
  type = string
}
variable "replica_db_skip_final_snapshot" {
  type = bool
}
variable "replica_db_allow_major_version_upgrade" {
  type = bool
}
variable "replica_db_auto_minor_version_upgrade" {
  type = bool
}
variable "replica_db_apply_immediately" {
  type = bool
}
variable "replica_db_backup_window" {
  type = string
}

# variable "replica_db_db_name" {
#   type = string
# }
# variable "replica_db_db_subnet_group_name" {
#   type = string
# }
# variable "replica_db_kms_key_id" {
#   type = string
# }
variable "replica_db_port" {
  type = string
}
variable "replica_db_multi_az" {
  type = bool
}
variable "replica_db_publicly_accessible" {
  type = bool
}

variable "replica_db_copy_tags_to_snapshot" {
  type = bool
}

variable "replica_db_delete_automated_backups" {
  type = bool
}
variable "replica_db_deletion_protection" {
  type = bool
}

variable "replica_db_monitoring_interval" {
  type = number
}

variable "replica_db_monitoring_role_arn" {
  type = string
}

variable "replica_db_performance_insights_enabled" {
  type = bool
}

variable "replica_db_performance_insights_kms_key_id" {
  type = string
}

variable "replica_db_performance_insights_retention_period" {
  type = number
}

variable "replica_db_storage_encrypted" {
  type = bool
}

# #role
variable "replica_db_rds_monitoring_role_name" {
  type = string
}

variable "replica_db_rds_monitoring_filename" {
  type = string
}

# variable "replica_db_parameter_group_name" {
#   type = string
# }

# variable "replica_db_option_group_name" {
#   type = string
# }


variable "replica_db_tags" {
  type = map(string)
}


#-------  OpenSearch   ------------------------------------------#


variable "opensearch_cluster_config" {
  type = list(object({
    domain                   = string
    elasticsearch_version    = string        
    instance_count           = number
    instance_type            = string
    zone_awareness_enabled   = bool
    dedicated_master_enabled = bool
    dedicated_master_count   = number
    dedicated_master_type    = string
    warm_instance_enabled    = bool 
    warm_instance_count      = number
    warm_instance_type       = string
    
    ebs_volume_size          = number
    ebs_volume_type          = string
    ebs_iops                 = number

    advanced_security_options       = bool
    internal_user_database_enabled  = bool
    master_user_name                = string
    master_user_password            = string

    custom_endpoint_enabled         = bool 
    custom_endpoint                 = string
    custom_endpoint_certificate_arn = string
 
    encrypt_at_rest_enabled         = bool
    encrypt_at_rest_kms_key_id      = string 
    node_to_node_encryption_enabled = bool
    automated_snapshot_start_hour   = number 
  }))
  default = [ ]
}

#------- secret-smanager  ------------------------------------------#

variable "secret_names" {
  type =  list(any)
}

variable "secret_files" {
  type =  list(any)
}

#------- Lambda  ------------------------------------------#


variable "iam_for_lambda_file" {
  type = string
}

variable "iam_policy_for_lambda_file" {
  type = string
}

variable "lambda-slack-zip" {
  type = string
}