module "vpc" {
  source               = "./modules/vpc-components/vpc"
  vpc_creation         = var.vpc_creation
  name                 = var.vpc_name
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = var.vpc_enable_dns_support
  enable_dns_hostnames = var.vpc_enable_dns_hostnames
  vpc_tags             = var.vpc_tags
  tags = merge(
    var.tags
  )
}

module "nat" {
  source           = "./modules/vpc-components/NAT"
  nat_create       = var.nat_create && var.create_private_subnets && length(var.private_subnets) > 0 ? true : false
  name             = var.nat_name
  public_subnet_id = var.nat_public_subnet_id != "" ? var.nat_public_subnet_id : module.public_subnets.public_subnets[0]
  nat_tags         = var.nat_tags
  tags = merge(
    var.tags
  )
}

module "private_subnets" {
  source                 = "./modules/vpc-components/private_subnets"
  name                   = var.private_subnets_name
  create_private_subnets = var.create_private_subnets
  private_subnets        = var.private_subnets
  vpc_id                 = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone      = var.availability_zone
  private_subnet_nat     = join(",", module.nat.nat_id)
  tags                   = var.tags
  private_subnets_tags   = var.private_subnets_tags
}

module "public_subnets" {
  source                = "./modules/vpc-components/public_subnets"
  create_public_subnets = var.create_public_subnets
  igw_create            = var.igw_create
  name                  = var.public_subnet_name
  public_subnets        = var.public_subnets
  vpc_id                = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone     = var.availability_zone
  tags                  = var.tags
  public_subnets_tags   = var.public_subnets_tags
}

module "database_subnets" {
  source                  = "./modules/vpc-components/database_subnets"
  name                    = var.database_subnets_name
  create_database_subnets = var.create_database_subnets
  database_subnets        = var.database_subnets
  vpc_id                  = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone       = var.availability_zone
  database_subnet_nat     = join(",", module.nat.nat_id)
  tags                    = var.tags
  database_subnets_tags   = var.database_subnets_tags
}

module "eks_subnets" {
  source             = "./modules/vpc-components/eks_subnets"
  name               = var.eks_subnets_name
  create_eks_subnets = var.create_eks_subnets
  eks_subnets        = var.eks_subnets
  vpc_id             = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  availability_zone  = var.availability_zone
  eks_subnet_nat     = join(",", module.nat.nat_id)
  tags               = var.tags
  eks_subnets_tags   = var.eks_subnets_tags
}

module "flow_logs_cloudwatch" {
  source                      = "./modules/vpc-components/flow_logs_cloudwatch"
  create_flow_logs_cloudwatch = var.create_flow_logs_cloudwatch
  vpc_id                      = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  name                        = var.flow_logs_cloudwatch_name
}

module "flow_logs_s3" {
  source              = "./modules/vpc-components/flow_logs_S3"
  create_flow_logs_s3 = var.create_flow_logs_s3
  vpc_id              = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  name                = var.flow_logs_s3_name
}

#####EKS

#EKS role for cluster
module "eks_cluster_role" {
  source            = "./modules/eks/eks_cluster_role"
  eks_role_creation = var.eks_role_creation
  name              = var.eks_role_name
  filename          = var.eks_role_filename
}

module "eks_nodegroup_role" {
  source                      = "./modules/eks/eks_nodegroup_role"
  eks_nodegroup_role_creation = var.eks_nodegroup_role_creation
  name                        = var.eks_nodegroup_role_name
  filename                    = var.eks_nodegroup_role_filename
}

module "eks_launch_template" {
  source                     = "./modules/eks/eks_launch_template"
  create_eks_launch_template = var.create_eks_launch_template
  name                       = var.eks_launch_template_name
  instance_type              = var.eks_launch_template_instance_type
  volume_size                = var.eks_launch_template_volume_size
  iops                       = var.eks_launch_template_iops
  volume_type                = var.eks_launch_template_volume_type
  delete_on_termination      = var.eks_launch_template_delete_on_termination
  encrypted                  = var.eks_launch_template_encrypted
  key_name                   = var.eks_launch_template_key_name
  kms_key_id                 = module.kms_key.kms_key_arn[0]
  # kms_key_id                 = "arn:aws:kms:ap-south-1:264086080894:key/3dc9290e-1c69-4b55-a94a-9e3c5aa75cbe"
  # kms_key_id                 = var.eks_kms_key_arn != "" ? var.eks_kms_key_arn : element(module.kms_key.kms_key_arn, 0)
  cluster_version            = var.eks_version
  cluster_name               = var.eks_cluster_name
  eks_cluster_endpoint       = module.eks_cluster.eks_clsuter_endpoint[0]
  eks_certificate_authority  =  module.eks_cluster.eks_certificate_authority
}

module "eks_cluster" {
  source                     = "./modules/eks/eks_cluster"
  eks_cluster_creation       = var.eks_cluster_creation
  name                       = var.eks_cluster_name
  role_arn                   = var.eks_role_arn != "" ? var.eks_role_arn : module.eks_cluster_role.iam_role_arn[0]
  eks_version                = var.eks_version
  subnet_ids                 = length(var.eks_subnet_ids) > 0 ? var.eks_subnet_ids : module.eks_subnets.eks_subnets
  endpoint_private_access    = var.eks_endpoint_private_access
  endpoint_public_access     = var.eks_endpoint_public_access
  key_arn                    = var.eks_kms_key_arn != "" ? var.eks_kms_key_arn : element(module.kms_key.kms_key_arn, 0)
  resources_to_encrypt       = var.eks_resources_to_encrypt
  retention_in_days          = var.eks_cluster_log_retention_days
  enabled_cluster_log_types  = var.eks_enabled_cluster_log_types
  create_eks_vpc_cni_addon   = var.create_eks_vpc_cni_addon
  create_eks_coredns_addon   = var.create_eks_coredns_addon
  create_eks_kubeproxy_addon = var.create_eks_kubeproxy_addon
  create_eks_ebscsi_addon    = var.create_eks_ebscsi_addon
  eks_cluster_tags           = var.eks_cluster_tags
  tags = merge(
    var.tags
  )
}



module "eks_nodegroup" {
  source                = "./modules/eks/eks_nodegroup"
  create_eks_node_group = var.create_eks_node_group
  #number_of_nodegroup   = var.number_of_nodegroup
  cluster_name            = var.eks_cluster_name != "" ? var.eks_cluster_name : module.eks_cluster.name[0]
  node_role_arn           = var.eks_nodegroup_role_arn != "" ? var.eks_nodegroup_role_arn : module.eks_nodegroup_role.iam_role_arn[0]
  nodegroup_config        = var.eks_nodegroup_config
  launch_template_id      = var.eks_nodegroup_launch_template_id != "" ? var.eks_nodegroup_launch_template_id : module.eks_launch_template.launch_template_id[0]
  launch_template_version = module.eks_launch_template.launch_template_version[0]
  subnet_ids              = length(var.eks_subnet_ids) > 0 ? var.eks_subnet_ids : module.eks_subnets.eks_subnets
}

### database

module "db_security_group" {
  source                 = "./modules/database/security_group_rds"
  create_rds_sg          = var.create_rds_sg
  name                   = var.rds_sg_name
  vpc_id                 = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  ingress_rules          = var.rds_sg_ingress_rules
  egress_rules           = var.rds_sg_egress_rules
  db_security_group_tags = var.rds_security_group_tags
  tags = merge(
    var.tags
  )
}

module "db_subnet_group" {
  source                 = "./modules/database/subnet_group"
  create_db_subnet_group = var.create_db_subnet_group
  name                   = var.db_subnet_group_name
  description            = var.db_subnet_group_description
  subnet_ids             = length(var.db_subnet_group_subnet_ids) > 0 ? var.db_subnet_group_subnet_ids : module.database_subnets.database_subnets[*]
  db_subnet_group_tags   = var.db_subnet_group_tags
  tags = merge(
    var.tags
  )
}


module "db_option_group" {
  source                   = "./modules/database/option_group"
  create_db_option_group   = var.create_db_option_group
  option_group_name        = var.db_option_group_name
  engine_name              = var.db_option_group_engine_name
  major_engine_version     = var.db_option_group_major_engine_version
  option_group_description = var.db_option_group_description
  option_group_options     = var.db_option_group_options
  db_option_group_tags     = var.db_option_group_tags
  tags = merge(
    var.tags
  )
}

module "db_parameter_group" {
  source                  = "./modules/database/parameter_group"
  create_parameter_group  = var.create_parameter_group
  name                    = var.db_parameter_group_name
  family                  = var.db_parameter_group_family
  parameters              = var.db_parameter_group_parameter
  db_parameter_group_tags = var.db_parameter_group_tags
  tags = merge(
    var.tags
  )
}


module "single_db_instance" {
  source                                            = "./modules/database/single_db_instance"
  db_instance_create_rds                            = var.db_instance_create_single_db_instance
  db_instance_allocated_storage                     = var.db_instance_allocated_storage
  db_instance_max_allocated_storage                 = var.db_instance_max_allocated_storage
  db_instance_storage_type                          = var.db_instance_storage_type
  db_instance_engine                                = var.db_instance_engine
  db_instance_engine_version                        = var.db_instance_engine_version
  db_instance_instance_class                        = var.db_instance_instance_class
  db_instance_identifier                            = var.db_instance_identifier
  db_instance_username                              = var.db_instance_username
  db_instance_password                              = var.db_instance_password
  db_instance_vpc_security_group_ids                = length(var.db_instance_vpc_security_group_ids) > 0 ? var.db_instance_vpc_security_group_ids : [element(module.db_security_group.rds_sg_ids, 0)]
  db_instance_maintenance_window                    = var.db_instance_maintenance_window
  db_instance_backup_retention_period               = var.db_instance_backup_retention_period
  db_instance_skip_final_snapshot                   = var.db_instance_skip_final_snapshot
  db_instance_allow_major_version_upgrade           = var.db_instance_allow_major_version_upgrade
  db_instance_auto_minor_version_upgrade            = var.db_instance_auto_minor_version_upgrade
  db_instance_apply_immediately                     = var.db_instance_apply_immediately
  db_instance_backup_window                         = var.db_instance_backup_window
  db_instance_publicly_accessible                   = var.db_instance_publicly_accessible
  db_instance_db_name                               = var.db_instance_db_name
  db_instance_db_subnet_group_name                  = var.db_instance_db_subnet_group_name != "" ? var.db_instance_db_subnet_group_name : element(module.db_subnet_group.db_subnet_group_name, 0)
  db_instance_port                                  = var.db_instance_port
  db_instance_multi_az                              = var.db_instance_multi_az
  db_instance_copy_tags_to_snapshot                 = var.db_instance_copy_tags_to_snapshot
  db_instance_delete_automated_backups              = var.db_instance_delete_automated_backups
  db_instance_parameter_group_name                  = var.db_instance_parameter_group_name != "" ? var.db_instance_parameter_group_name : element(module.db_parameter_group.parameter_group_name, 0)
  db_instance_option_group_name                     = var.db_instance_option_group_name != "" ? var.db_instance_option_group_name : element(module.db_option_group.db_option_group_name, 0)
  db_instance_kms_key_id                            = var.db_instance_kms_key_id != "" ? var.db_instance_kms_key_id : element(module.kms_key.kms_key_arn, 1)
  db_instance_rds_monitoring_role_name              = var.db_instance_rds_monitoring_role_name #
  db_instance_deletion_protection                   = var.db_instance_deletion_protection
  db_instance_monitoring_interval                   = var.db_instance_monitoring_interval #
  db_instance_monitoring_role_arn                   = var.db_instance_monitoring_role_arn #
  db_instance_performance_insights_enabled          = var.db_instance_performance_insights_enabled
  db_instance_performance_insights_kms_key_id       = var.db_instance_performance_insights_kms_key_id != "" ? var.db_instance_performance_insights_kms_key_id : element(module.kms_key.kms_key_arn, 1)
  db_instance_performance_insights_retention_period = var.db_instance_performance_insights_retention_period
  db_instance_storage_encrypted                     = var.db_instance_storage_encrypted
  rds_monitoring_filename                           = var.db_instance_rds_monitoring_filename
  single_db_instance_tags                           = var.db_instance_tags
  tags = merge(
    var.tags
  )
}

module "rds_read_replica" {
  source                                           = "./modules/database/read_replica"
  replica_db_create                                = var.replica_db_create
  replica_db_count                                 = var.replica_db_count
  replica_db_replicate_source_db                   = var.replica_db_replicate_source_db != "" ? var.replica_db_replicate_source_db : module.single_db_instance.rds_instance_identifer[0]
  replica_db_allocated_storage                     = var.replica_db_allocated_storage
  replica_db_max_allocated_storage                 = var.replica_db_max_allocated_storage
  replica_db_storage_type                          = var.replica_db_storage_type
  replica_db_instance_class                        = var.replica_db_instance_class
  replica_db_identifier                            = var.replica_db_identifier
  replica_db_vpc_security_group_ids                = length(var.replica_db_vpc_security_group_ids) > 0 ? var.replica_db_vpc_security_group_ids : [element(module.db_security_group.rds_sg_ids, 0)]
  replica_db_maintenance_window                    = var.replica_db_maintenance_window
  replica_db_backup_retention_period               = var.replica_db_backup_retention_period
  replica_db_skip_final_snapshot                   = var.replica_db_skip_final_snapshot
  replica_db_allow_major_version_upgrade           = var.replica_db_allow_major_version_upgrade
  replica_db_auto_minor_version_upgrade            = var.replica_db_auto_minor_version_upgrade
  replica_db_apply_immediately                     = var.replica_db_apply_immediately
  replica_db_backup_window                         = var.replica_db_backup_window
  replica_db_publicly_accessible                   = var.replica_db_publicly_accessible
  replica_db_multi_az                              = var.replica_db_multi_az
  replica_db_db_subnet_group_name                  = var.db_instance_db_subnet_group_name != "" ? var.db_instance_db_subnet_group_name : element(module.db_subnet_group.db_subnet_group_name, 0)
  replica_db_kms_key_id                            = var.db_instance_kms_key_id != "" ? var.db_instance_kms_key_id : element(module.kms_key.kms_key_arn, 1)
  replica_db_port                                  = var.replica_db_port
  replica_db_copy_tags_to_snapshot                 = var.replica_db_copy_tags_to_snapshot
  replica_db_delete_automated_backups              = var.replica_db_delete_automated_backups
  replica_db_deletion_protection                   = var.replica_db_deletion_protection
  replica_db_monitoring_interval                   = var.replica_db_monitoring_interval
  replica_db_monitoring_role_arn                   = var.replica_db_monitoring_role_arn
  replica_db_parameter_group_name                  = var.db_instance_parameter_group_name != "" ? var.db_instance_parameter_group_name : element(module.db_parameter_group.parameter_group_name, 0)
  replica_db_option_group_name                     = var.db_instance_option_group_name != "" ? var.db_instance_option_group_name : element(module.db_option_group.db_option_group_name, 0)
  replica_db_performance_insights_enabled          = var.replica_db_performance_insights_enabled
  replica_db_performance_insights_kms_key_id       = var.replica_db_performance_insights_kms_key_id != "" ? var.replica_db_performance_insights_kms_key_id : element(module.kms_key.kms_key_arn, 1)
  replica_db_performance_insights_retention_period = var.replica_db_performance_insights_retention_period
  replica_db_monitoring_role_name                  = var.replica_db_rds_monitoring_role_name
  replica_db_monitoring_filename                   = var.replica_db_rds_monitoring_filename
  replica_db_storage_encrypted                     = var.replica_db_storage_encrypted
  replica_db_tags                                  = var.replica_db_tags
  tags = merge(
    var.tags
  )
}

### OpenSearch 
module "openSerch" {
  source            = "./modules/openSearch"
  public_subnet     = var.public_subnets
  public_subnet_ids = module.public_subnets.public_subnet_ids
  opensearch_cluster_config = var.opensearch_cluster_config
}

## Create secret-manager
module "secret-manager" {
  source = "./modules/secret-manager"
  secret_names = var.secret_names
  secret_files = var.secret_files 
}

### lambda 
module "lambda" {
  source = "./modules/lambda" 
  iam_for_lambda_file         = var.iam_for_lambda_file
  iam_policy_for_lambda_file  = var.iam_policy_for_lambda_file
  lambda-slack-zip            = var.lambda-slack-zip
}


### Create a KMS key CMK
module "kms_key" {
  source          = "./modules/kms"
  create_kms_keys = var.create_kms_keys
  key_aliases     = var.key_aliases
  kms_policy = var.kms_policy
  #eks_role = module.eks_cluster_role.iam_role_arn[0]
}

### Create S3 bucket 
module "s3_bucket" {
  source        = "./modules/s3"
  bucket_config = var.bucket_config  
  kms_key_ids   =  module.kms_key.kms_key_ids
  kms_key_arn   =  module.kms_key.kms_key_arn
}

### Create sqs 
module "sqs" {
  source           = "./modules/sqs"
  sqs_queue_config = var.sqs_queue_config
  kms_key_ids      =  module.kms_key.kms_key_ids
  kms_key_arn      =  module.kms_key.kms_key_arn
}

### ec2-sg
module "ec2-sg" {
  source = "./modules/ec2/security-group"
  vpc_id = var.vpc_id != "" ? var.vpc_id : module.vpc.vpc_id[0]
  name   = var.sg-name  
}

### Create a ec2 
module "ec2" {
  source        = "./modules/ec2"
  # public_subnet      = module.public_subnets.public_subnets_id[0]
  kms_key_arn        = module.kms_key.kms_key_arn
  userdata_file      = var.userdata_file 
  public-instance-sg = module.ec2-sg.public_sg_id
  ec2_instances      = var.ec2_instances
}

#ec2
# module "ec2" {
#   source                           = "./modules/ec2"
#   ec2_instance_create              = var.ec2_instance_create
#   instance_count                   = var.ec2_instance_instance_count
#   ami_id                           = var.ec2_instance_ami_id
#   instance_type                    = var.ec2_instance_instance_type
#   key_pair_name                    = var.ec2_instance_key_pair_name
#   subnet_id                        = var.ec2_instance_subnet_id
#   security_group_id                = var.ec2_instance_security_group_id
#   associate_public_ip_address      = var.ec2_instance_associate_public_ip_address
#   user_data                        = var.ec2_instance_user_data
#   ebs_optimized                    = var.ec2_instance_ebs_optimized
#   monitoring                       = var.ec2_instance_monitoring
#   root_block_delete_on_termination = var.ec2_instance_root_block_delete_on_termination
#   kms_key_id                       = var.ec2_instance_kms_key_id
#   root_block_volume_size           = var.ec2_instance_root_block_volume_size
#   root_block_volume_type           = var.ec2_instance_root_block_volume_type
#   ebs_block_delete_on_termination  = var.ec2_instance_ebs_block_delete_on_termination
#   ebs_block_device_name            = var.ec2_instance_ebs_block_device_name
#   ebs_block_encrypted              = var.ec2_instance_ebs_block_encrypted
#   ebs_block_volume_size            = var.ec2_instance_ebs_block_volume_size
#   ebs_block_volume_type            = var.ec2_instance_ebs_block_volume_type
# }