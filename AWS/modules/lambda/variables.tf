
variable "iam_for_lambda_file" {
  type = string
}

variable "iam_policy_for_lambda_file" {
  type = string
}

variable "lambda-slack-zip" {
  type = string
}

