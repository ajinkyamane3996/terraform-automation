resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = file(var.iam_for_lambda_file)

}

resource "aws_iam_policy" "iam_policy_for_lambda" {
 
 name         = "aws_iam_policy_for_terraform_aws_lambda_role"
 path         = "/"
 description  = "AWS IAM Policy for managing aws lambda role"
 policy = file(var.iam_policy_for_lambda_file)

}

resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
 role = aws_iam_role.iam_for_lambda.name
 policy_arn  = aws_iam_policy.iam_policy_for_lambda.arn
}
resource "aws_lambda_function" "terraform_lambda_func" {
filename                       = var.lambda-slack-zip
function_name                  = "lambda-slack-notify"
role                           = aws_iam_role.iam_for_lambda.arn
handler                        = "index.lambda_handler"
runtime                        = "python3.8"
depends_on                     = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}
