
data "template_file" "user_data" {
  template = "${file("./modules/ec2/userdata.sh")}"
}

resource "aws_instance" "ec2_instance" {
  count = length(var.ec2_instances)
  
  ami                    = var.ec2_instances[count.index].ami
  instance_type          = var.ec2_instances[count.index].instance_type
  vpc_security_group_ids = [var.public-instance-sg]

  root_block_device {
    volume_type           = var.ec2_instances[count.index].volume_type
    volume_size           = var.ec2_instances[count.index].volume_size
    delete_on_termination = var.ec2_instances[count.index].delete_on_termination_rblk
    kms_key_id            = var.kms_key_arn[0]
  }

  dynamic "ebs_block_device" {
    for_each = var.ec2_instances[count.index].ebs_block_devices
    content {
      delete_on_termination = ebs_block_device.value.delete_on_termination_ebsblk
      device_name           = ebs_block_device.value.device_name
      volume_size           = ebs_block_device.value.volume_size
      volume_type           = ebs_block_device.value.volume_type
      encrypted             = ebs_block_device.value.encrypted
      kms_key_id            = var.kms_key_arn[0]
    }
  }

  tags = {
    # Name = "example-instance-0${count.index + 1}"
    Name         = var.ec2_instances[count.index].tags[0]
    Environment  = var.ec2_instances[count.index].tags[1]
    Project      = var.ec2_instances[count.index].tags[2]
    ResourceName = var.ec2_instances[count.index].tags[3]
    Managed-by   = var.ec2_instances[count.index].tags[4]
  }

  subnet_id = var.ec2_instances[count.index].subnet_id
  key_name  = var.ec2_instances[count.index].key_name
  user_data = "${data.template_file.user_data.rendered}"
}
