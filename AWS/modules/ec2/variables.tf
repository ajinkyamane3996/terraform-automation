variable "ec2_instances" {
  type = list(object({
    ami           = string
    instance_type = string    
    volume_type   = string
    volume_size   = number
    delete_on_termination_rblk = bool
    ebs_block_devices = list(object({
      delete_on_termination_ebsblk = bool
      device_name                  = string
      volume_size                  = number
      volume_type                  = string
      encrypted                    = bool
    }))
    # delete_on_termination_ebsblk = bool
    # device_name   = string
    # volume_type   = string
    # volume_size   = number
    # encrypted     = bool    
    key_name      = string
    tags          = list(string)
    subnet_id     = string
  }))
  default = [ ]
}

variable "vpc_id" {
  default = ""
}

# variable "public_subnet" {
#   default = []
# }

variable "kms_key_arn" {
  type = list(string)  
  default = []
}

variable "kms_key_ids" { 
  type = list(string)  
  default = []
} 

variable "userdata_file" {
  type = string
}

variable "name" {
  type    = string
  default = "pub-sg-ssh"
}

variable "public-instance-sg" {
  description = "A list of security group IDs to associate with"
  type        = string
  default     = null
}
