#!/usr/bin/env bash

# Remove apparmor and snapd
apt-get -y purge apparmor
apt-get -y purge snapd
apt-get -y update
apt-get -y upgrade


# install utility tools
sudo apt-get -y update
sudo apt-get -y install \
    vim \
    nano 

# install sysmonitoring packages

apt-get -y install \
    glances \
    lsof \
    tcpdump \
    sysstat \
    iftop

# Configuring sysstat package

echo -e "# The first element of the path is a \n# directory where the debian-sa1 \n# script is located \nPATH=/usr/lib/sysstat:/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin \n# Activity reports every 10 minutes everyday \n*/2 * * * * root command -v debian-sa1 > /dev/null && debian-sa1 1 1 \n # Additional run at 23:59 to rotate the statistics file \n59 23 * * * root command -v debian-sa1 > /dev/null && debian-sa1 60 2" > /etc/default/sysstat


# Run unattended upgrades
unattended-upgrade -d

# Remove x11-common
apt-get -y purge x11-common

# Remove open-iscsi
apt-get -y purge open-iscsi

# Remove mdadm
apt-get -y purge mdadm

# Remove apport
apt-get -y purge apport

# Remove lvm2
apt-get -y purge lvm2

# Remove hddtemp
apt-get -y purge hddtemp

# Remove lxcfs
apt-get -y purge lxcfs

# Remove lm-sensors
apt-get -y purge lm-sensors

###update ssh file ###

sudo sed -i 's/X11Forwarding yes/X11Forwarding no/' /etc/ssh/sshd_config

sudo echo "PermitRootLogin no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "ClientAliveInterval 300" | sudo tee -a /etc/ssh/sshd_config

sudo echo "ClientAliveCountMax 3" | sudo tee -a /etc/ssh/sshd_config

sudo echo "MaxAuthTries 3" | sudo tee -a /etc/ssh/sshd_config

sudo echo "IgnoreRhosts yes" | sudo tee -a /etc/ssh/sshd_config

sudo echo "HostBasedAuthentication no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "GSSAPIAuthentication no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "KerberosAuthentication no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "Protocol 2" | sudo tee -a /etc/ssh/sshd_config

sudo echo "HostKey /etc/ssh/ssh_host_rsa_key" | sudo tee -a /etc/ssh/sshd_config

sudo echo "HostKey /etc/ssh/ssh_host_ed25519_key" | sudo tee -a /etc/ssh/sshd_config

sudo echo "SyslogFacility AUTH" | sudo tee -a /etc/ssh/sshd_config

sudo echo "LogLevel INFO" | sudo tee -a /etc/ssh/sshd_config

sudo echo "PermitEmptyPasswords no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "AllowTcpForwarding no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "PrintLastLog yes" | sudo tee -a /etc/ssh/sshd_config

sudo echo "PermitTunnel no" | sudo tee -a /etc/ssh/sshd_config

sudo echo "Banner /etc/motd" | sudo tee -a /etc/ssh/sshd_config

sudo echo "PrintLastLog yes" | sudo tee -a /etc/ssh/sshd_config

sudo echo -e "# Restrict key exchange, cipher, and MAC algorithms, as per sshaudit.com\n# hardening guide.\nKexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256\nciphers aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr\nmacs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com" | sudo tee -a /etc/ssh/sshd_config

sed -i 's/^PASS_MAX_DAYS.*/PASS_MAX_DAYS   90/g' /etc/login.defs

sed -i 's/^PASS_MIN_DAYS.*/PASS_MIN_DAYS   7/g' /etc/login.defs

sed -i 's/^PASS_WARN_AGE.*/PASS_WARN_AGE   7/g' /etc/login.defs

sudo echo "INACTIVE=30" | sudo tee -a /etc/default/useradd

sed -i '27d' /etc/pam.d/common-password

sed -i '27 i password requisite pam_cracklib.so retry=3 minlen=8 dcredit=-1 ucredit=-1 ocredit=-1 lcredit=-1' /etc/pam.d/common-password

sudo echo "password sufficient pam_unix.so remember=5" | sudo tee -a /etc/pam.d/common-password

sed -i '19d' /etc/pam.d/common-auth

sed -i '19 i auth        required      pam_tally2.so deny=3 unlock_time=1800' /etc/pam.d/common-auth

sudo apt-get install -y auditd 

touch /etc/audit/rules.d/tpaudit.rules

sudo echo -e "#To monitor user group info\n -w /etc/group -p wa -k identity\n -w /etc/passwd -p wa -k identity\n -w /etc/gshadow -p wa -k identity\n -w /etc/shadow -p wa -k identity\n -w /etc/security/opasswd -p wa -k identity\n #Collect Login/Logout Events\n -w /var/log/faillog -p wa -k logins\n -w /var/log/lastlog -p wa -k logins\n -w /var/log/tallylog -p wa -k logins\n #Collect Session Initiation Information\n -w /var/run/utmp -p wa -k session\n -w /var/log/wtmp -p wa -k session\n -w /var/log/btmp -p wa -k session\n #Collect Discretionary Access Control Permission Modification Events\n -a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=500 -F auid!=4294967295 -k perm_mod\n -a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=500 -F auid!=4294967295 -k perm_mod\n -a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=500 -F auid!=4294967295 -k perm_mod\n -a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=500 -F auid!=4294967295 -k perm_mod\n -a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod\n -a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod\n #Collect Unsuccessful Unauthorized Access Attempts to Files\n -a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=500 -F auid!=4294967295 -k access\n -a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EACCES -F auid>=500 -F auid!=4294967295 -k access\n -a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=500 -F auid!=4294967295 -k access\n -a always,exit -F arch=b32 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-EPERM -F auid>=500 -F auid!=4294967295 -k access\n #Collect File Deletion Events by User\n -a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=500 -F auid!=4294967295 -k delete\n -a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=500 -F auid!=4294967295 -k delete\n #Collect System Administrator Actions (sudolog)\n -w /var/log/sudo.log -p wa -k actions" | sudo tee -a /etc/audit/rules.d/tpaudit.rules


