# // Userdata
# data template_file "user_data" {
#   template = "${file("./ec2/userdata.sh")}"
# }

# resource "aws_instance" "example" {
#   ami           = "ami-0d5d9d301c853a04a"
#   instance_type = "t2.micro"

#   root_block_device {
#     volume_type           = "gp2"
#     volume_size           = 8
#     delete_on_termination = true
#   }

#   ebs_block_device {
#     delete_on_termination = true
#     device_name = "/dev/sdb"
#     volume_size = 20
#     volume_type = "gp3"
#     encrypted   = true
#     kms_key_id  = var.kms_key_arn[0]
#   }

#   ebs_block_device {
#     delete_on_termination = true
#     device_name = "/dev/sdc"
#     volume_size = 20
#     volume_type = "gp3"
#     encrypted   = true
#     kms_key_id  = var.kms_key_arn[0]
#   }

#   tags = {
#     Name = "example-instance"
#   }


#   subnet_id = var.public_subnet
#   key_name = "example-key-pair"
#   user_data_base64 = base64encode(data.template_file.user_data.rendered)

# }

# resource "aws_security_group" "example" {
#   name        = "example"
#   description = "Allow HTTP traffic"

#   ingress {
#     from_port = 80
#     to_port   = 80
#     protocol = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
# }

#   egress {   
#     from_port = 0
#     to_port = 0
#     protocol = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
# }
# }


