resource "aws_kms_key" "demokmskey" {
  count                    = var.create_kms_keys && length(var.key_aliases) > 0 ? length(var.key_aliases) : 0
  description              = var.key_aliases[count.index].key_description
  customer_master_key_spec = var.key_aliases[count.index].key_spec
  is_enabled               = var.key_aliases[count.index].enabled
  enable_key_rotation      = var.key_aliases[count.index].rotation_enabled
  deletion_window_in_days  = var.key_aliases[count.index].deletion_window
  policy                   = var.key_aliases[count.index].kms_policy_enabled ? file(var.kms_policy) : null
}

resource "aws_kms_alias" "demokmskey" {
  count         = var.create_kms_keys && length(var.key_aliases) > 0 ? length(var.key_aliases) : 0
  name          = var.key_aliases[count.index].key_alias
  target_key_id = aws_kms_key.demokmskey[count.index].key_id
}

