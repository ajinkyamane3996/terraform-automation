variable "key_aliases" {
  type = list(object({
    key_alias        = string
    key_description  = string
    key_spec         = string
    enabled          = bool
    rotation_enabled = bool
    deletion_window  = number
    kms_policy_enabled = bool
  }))
  default = []
}

variable "create_kms_keys" {
  type = bool
}


variable "kms_policy" {
  type = string
}

variable "alias" {
  default = ""
  type    = string
}

variable "target_key_id" {
  default = ""
  type    = string
}

variable "event" {
  description = "owner of resource"
  default     = true
  type        = bool
}


