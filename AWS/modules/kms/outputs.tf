output "kms_key_arn" {
  description = "aws kms key alias"
  value       = [for i in aws_kms_alias.demokmskey : i.arn]
  #value = tostring(aws_kms_alias.demokmskey[0].arn)
}

output "kms_key_ids" {
  # value = values(aws_kms_key.demokmskey[*].key_id)
  value = [for i in aws_kms_key.demokmskey : i.key_id]
}
