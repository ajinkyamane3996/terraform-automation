resource "aws_secretsmanager_secret" "tf_multiple_secrets" {
  count = length(var.secret_names)
  name  = var.secret_names[count.index]
}

resource "aws_secretsmanager_secret_version" "secret1" {
  count         = length(var.secret_names)
  secret_id     = aws_secretsmanager_secret.tf_multiple_secrets[count.index].id
  secret_string = file(var.secret_files[count.index])  
}





