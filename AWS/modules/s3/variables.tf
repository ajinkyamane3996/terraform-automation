variable "kms_key_ids" { 
  type = list(string)  
  default = []
} 

variable "kms_key_arn" {
  type = list(string)  
  default = []
}

variable "bucket_config" {
  type = list(object({
    bucket_name = string
    enable_versioning= bool
    enable_server_side_encryption= bool
    enable_lifecycle_rule= bool
    days = number
    storage_class= string
    noncurrent_days = number
    noncurrent_storage_class= string
  }))
  default = [ ]
}

