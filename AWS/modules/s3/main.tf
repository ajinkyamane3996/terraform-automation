# Create an s3 bucket and encrypt it using the KMS key
resource "aws_s3_bucket" "example" {
  count = length(var.bucket_config)
  bucket = var.bucket_config[count.index].bucket_name
}

resource "aws_s3_bucket_versioning" "example" {
  count = length(var.bucket_config)
  # bucket = aws_s3_bucket.example.id
  bucket = aws_s3_bucket.example[count.index].id 

  versioning_configuration {
    status = var.bucket_config[count.index].enable_versioning ? "Enabled" : "Suspended"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "example" {
  count = length(var.kms_key_ids)
  # count = length(var.bucket_config)
  # bucket = aws_s3_bucket.example.id
  bucket = aws_s3_bucket.example[count.index].id  
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = var.kms_key_ids[1]
      # kms_master_key_id = var.kms_key_arn[1]
      # kms_master_key_id = aws_kms_key_alias.demokmskey[find(var.key_aliases[*].key_alias, "alias/s3bucket")].name
      sse_algorithm     = "aws:kms"
      # sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "example" {
  count = length(var.bucket_config)
  # bucket = aws_s3_bucket.example.id
  bucket = aws_s3_bucket.example[count.index].id 
  rule {
    id = "example-rule-${count.index}"
    status = var.bucket_config[count.index].enable_lifecycle_rule ? "Enabled" : "Disabled"
    transition {
      days          = var.bucket_config[count.index].days
      storage_class = var.bucket_config[count.index].storage_class
    }
    transition {
      days          = var.bucket_config[count.index].days
      storage_class = var.bucket_config[count.index].storage_class
    }
    noncurrent_version_transition {
      noncurrent_days = var.bucket_config[count.index].noncurrent_days
      storage_class   = var.bucket_config[count.index].noncurrent_storage_class
    }
    noncurrent_version_expiration {
      noncurrent_days = var.bucket_config[count.index].noncurrent_days
    }
  }
}




