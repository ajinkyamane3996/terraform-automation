# Creating the Elasticsearch domain
resource "aws_elasticsearch_domain" "es" {

  count = length(var.opensearch_cluster_config)

  domain_name           = var.opensearch_cluster_config[count.index].domain
  elasticsearch_version = var.opensearch_cluster_config[count.index].elasticsearch_version

  advanced_options = var.advanced_options

  cluster_config {

    instance_count           = var.opensearch_cluster_config[count.index].instance_count
    instance_type            = var.opensearch_cluster_config[count.index].instance_type
  # zone_awareness_enabled   = (var.availability_zones > 1) ? true : false 
    zone_awareness_enabled   = var.opensearch_cluster_config[count.index].zone_awareness_enabled
   
    dedicated_master_enabled = var.opensearch_cluster_config[count.index].dedicated_master_enabled
    dedicated_master_count   = var.opensearch_cluster_config[count.index].dedicated_master_enabled ? var.opensearch_cluster_config[count.index].dedicated_master_count : null
    dedicated_master_type    = var.opensearch_cluster_config[count.index].dedicated_master_enabled ? var.opensearch_cluster_config[count.index].dedicated_master_type : null
    
    warm_enabled             = var.opensearch_cluster_config[count.index].warm_instance_enabled
    warm_count               = var.opensearch_cluster_config[count.index].warm_instance_enabled ? var.opensearch_cluster_config[count.index].warm_instance_count : null
    warm_type                = var.opensearch_cluster_config[count.index].warm_instance_enabled ? var.opensearch_cluster_config[count.index].warm_instance_type : null

  }

#  zone_awareness_config { availability_zone_count = 2 }

  ebs_options {
    ebs_enabled = var.opensearch_cluster_config[count.index].ebs_volume_size > 0 ? true : false
    volume_size = var.opensearch_cluster_config[count.index].ebs_volume_size
    volume_type = var.opensearch_cluster_config[count.index].ebs_volume_type
    iops        = var.opensearch_cluster_config[count.index].ebs_iops
  }

  vpc_options {
    # subnet_ids = ["subnet-0a31ab9be90841156"]
    subnet_ids = [
      var.public_subnet_ids[0],
      var.public_subnet_ids[1],
    ]
  }

## --------------------------------------------------------

  advanced_security_options {
    enabled = var.opensearch_cluster_config[count.index].advanced_security_options
    internal_user_database_enabled = var.opensearch_cluster_config[count.index].internal_user_database_enabled
    master_user_options {
       master_user_name = var.opensearch_cluster_config[count.index].master_user_name
       master_user_password = var.opensearch_cluster_config[count.index].master_user_password
     }
  }

  domain_endpoint_options {
    enforce_https = true
    custom_endpoint_enabled = var.opensearch_cluster_config[count.index].custom_endpoint_enabled
    custom_endpoint = var.opensearch_cluster_config[count.index].custom_endpoint_enabled ? var.opensearch_cluster_config[count.index].custom_endpoint : null
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
    custom_endpoint_certificate_arn = var.opensearch_cluster_config[count.index].custom_endpoint_enabled ? var.opensearch_cluster_config[count.index].custom_endpoint_certificate_arn : null
  }

## --------------------------------------------------------

 encrypt_at_rest {
    enabled    = var.opensearch_cluster_config[count.index].encrypt_at_rest_enabled
    # kms_key_id = var.opensearch_cluster_config[count.index].encrypt_at_rest_kms_key_id
  }  

  node_to_node_encryption {
    enabled = var.opensearch_cluster_config[count.index].node_to_node_encryption_enabled
  }

  snapshot_options {
    automated_snapshot_start_hour = var.opensearch_cluster_config[count.index].automated_snapshot_start_hour
  }
     
  tags = {  
    # Domain = var.tag_domain  
    Domain = "ES-Logging-Domain" 
  }

}


# Creating the AWS Elasticsearch domain policy
 
resource "aws_elasticsearch_domain_policy" "main" {
  
  count = length(var.opensearch_cluster_config)

  domain_name = aws_elasticsearch_domain.es[count.index].domain_name
  access_policies = file("./files/elasticsearch_policy.json")

#   access_policies = <<POLICIES
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Action": "es:*",
#             "Principal": "*",
#             "Effect": "Allow",
#             "Resource": "${aws_elasticsearch_domain.es.arn}/*"
#         }
#     ]
# }
# POLICIES
}
