# variable "domain" {
#     type = string
#     default = "newdomain"
#     description = "name of Elasticsearch Domain"
# }

# variable "elasticsearch_version" {
#   type        = string
#   default     = "7.10"
#   description = "Version of Elasticsearch to deploy"
# }

# variable "instance_type" {
#     type = string
#     default     = "t3.small.elasticsearch"
#     # default = "r4.large.elasticsearch"
#     description = "Elasticsearch instance type for data nodes in the cluster"
# }

# variable "instance_count" {
#   type        = number
#   description = "Number of data nodes in the cluster"
#   default     = 2
# }

# variable "dedicated_master_enabled" {
#   type        = bool
#   default     = false
#   description = "Indicates whether dedicated master nodes are enabled for the cluster"
# }

# variable "dedicated_master_count" {
#   type        = number
#   description = "Number of dedicated master nodes in the cluster"
#   default     = 2
# }

# variable "dedicated_master_type" {
#   type        = string
#   default     = "t3.small.elasticsearch"
#   description = "Instance type of the dedicated master nodes in the cluster"
# }

# variable "warm_instance_enabled" {
#   description = "Indicates whether ultrawarm nodes are enabled for the cluster."
#   type        = bool
#   default     = false
# }

# variable "warm_instance_type" {
#   description = "The type of EC2 instances to run for each warm node. A list of available instance types can you find at https://aws.amazon.com/en/elasticsearch-service/pricing/#UltraWarm_pricing"
#   type        = string
#   default     = "ultrawarm1.large.elasticsearch"
# }

# variable "warm_instance_count" {
#   description = "The number of dedicated warm nodes in the cluster."
#   type        = number
#   default     = 2
# }

# variable "zone_awareness_enabled" {
#   type        = bool
#   default     = true
#   description = "Enable zone awareness for Elasticsearch cluster"
# }

# # variable "availability_zones" {
# #   description = "The number of availability zones for the OpenSearch cluster. Valid values: 1, 2 or 3."
# #   type        = number
# #   default     = 3
# # }

# variable "node_to_node_encryption_enabled" {
#   type        = bool
#   default     = true
#   description = "Whether to enable node-to-node encryption"
# }


# variable "automated_snapshot_start_hour" {
#   type        = number
#   default     = 23
#   description = "automated snapshot start hour"
# }


# variable "tag_domain" {
#     type = string
#     default = "NewDomain"
# }

# variable "ebs_volume_size" {
#     type = number
#     default = 10
#     description = "EBS volumes for data storage in GB"
# }

# variable "ebs_volume_type" {
#   type        = string
#   default     = "gp2"
#   description = "Storage type of EBS volumes"
# }

# variable "ebs_iops" {
#   type        = number
#   default     = 0
#   description = "The baseline input/output (I/O) performance of EBS volumes attached to data nodes. Applicable only for the Provisioned IOPS EBS volume type"
# }

# variable "encrypt_at_rest_enabled" {
#   type        = bool
#   default     = true
#   description = "Whether to enable encryption at rest"
# }

####------------------------------------------------------------------------------------

# variable "advanced_security_options" {
#   type        = bool
#   description = "Whether to enable advanced_security_options."
#   default     = true
# }

# variable "internal_user_database_enabled" {
#   type        = bool
#   description = "Whether to enable internal_user_database_enabled."
#   default     = true
# }

# variable "custom_endpoint_enabled" {
#   type        = bool
#   description = "Whether to enable custom endpoint for the Elasticsearch domain."
#   default     = false
# }

# variable "custom_endpoint" {
#   type        = string
#   description = "Fully qualified domain for custom endpoint."
#   default     = "kibana-new.preprod.internal.getvokal.com"
# }

# variable "custom_endpoint_certificate_arn" {
#   type        = string
#   description = "ACM certificate ARN for custom endpoint."
#   default     = "arn:aws:acm:ap-south-1:852303623836:certificate/34cddb18-6e66-47ac-a94a-8aa8b32c73f9"
# }

# variable "master_user_name" {
#   type        = string
#   default     = "esadmin"
#   description = "Master user username (applicable if advanced_security_options_internal_user_database_enabled set to true)"
# }

# variable "master_user_password" {
#   type        = string
#   default     = "ESadmin@5678910"
#   description = "Master user password (applicable if advanced_security_options_internal_user_database_enabled set to true)"
# }

####------------------------------------------------------------------------------------


variable "advanced_options" {
  type        = map(string)
  default     = {}
  description = "Key-value string pairs to specify advanced configuration options"
}

variable "encrypt_at_rest_kms_key_id" {
  type        = string
  default     = ""
  description = "The KMS key ID to encrypt the Elasticsearch domain with. If not specified, then it defaults to using the AWS/Elasticsearch service KMS key"
}

variable "public_subnet" {
  default = []
}

variable "public_subnet_ids" { 
  type = list(string)  
  default = []
} 

variable "opensearch_cluster_config" {
  type = list(object({
    domain                   = string
    elasticsearch_version    = string    
    # advanced_options         = list(string)
    
    instance_count           = number
    instance_type            = string
    zone_awareness_enabled   = bool
    dedicated_master_enabled = bool
    dedicated_master_count   = number
    dedicated_master_type    = string
    warm_instance_enabled    = bool 
    warm_instance_count      = number
    warm_instance_type       = string
    
    ebs_volume_size          = number
    ebs_volume_type          = string
    ebs_iops                 = number

    advanced_security_options       = bool
    internal_user_database_enabled  = bool
    master_user_name                = string
    master_user_password            = string

    custom_endpoint_enabled         = bool 
    custom_endpoint                 = string
    custom_endpoint_certificate_arn = string
 
    encrypt_at_rest_enabled         = bool
    encrypt_at_rest_kms_key_id      = string 
    node_to_node_encryption_enabled = bool
    automated_snapshot_start_hour   = number 
  }))
  default = [ ]
}


