variable "eks_role_creation" {
  type = bool
}

variable "name" {
  type = string
}

variable "filename" {
  type = string
}