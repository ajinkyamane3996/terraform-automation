resource "aws_iam_role" "cluster_role" {
  count = var.eks_role_creation ? 1 : 0
  name  = var.name

  assume_role_policy = file(var.filename)

}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  count      = var.eks_role_creation ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster_role[count.index].name
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSVPCResourcePolicy" {
  count      = var.eks_role_creation ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.cluster_role[count.index].name
}
