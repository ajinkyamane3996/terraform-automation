# EKS Cluster
resource "aws_eks_cluster" "eks_cluster" {
  count    = var.eks_cluster_creation ? 1 : 0
  name     = var.name
  role_arn = var.role_arn
  version  = var.eks_version

  vpc_config {
    #security_group_ids      = var.security_group_ids
    subnet_ids              = var.subnet_ids
    endpoint_private_access = var.endpoint_private_access
    endpoint_public_access  = var.endpoint_public_access
  }

  encryption_config {
    provider {
      key_arn = var.key_arn
    }
    resources = var.resources_to_encrypt
  }
  enabled_cluster_log_types = var.enabled_cluster_log_types

  tags = merge(
    var.tags,
    var.eks_cluster_tags
  )
}


resource "aws_cloudwatch_log_group" "eks_log_group" {
  count             = var.eks_cluster_creation ? 1 : 0
  name              = "/aws/eks/${var.name}/cluster"
  retention_in_days = var.retention_in_days
}

resource "aws_eks_addon" "eks_vpc_cni_addon" {
  count        = var.create_eks_vpc_cni_addon ? 1 : 0
  cluster_name = aws_eks_cluster.eks_cluster[0].name
  addon_name   = "vpc-cni"
  depends_on = [
    aws_eks_cluster.eks_cluster
  ]
}

resource "aws_eks_addon" "eks_coredns_addon" {
  count        = var.create_eks_coredns_addon ? 1 : 0
  cluster_name = aws_eks_cluster.eks_cluster[0].name
  addon_name   = "coredns"
  depends_on = [
    aws_eks_cluster.eks_cluster
  ]
}

resource "aws_eks_addon" "eks_kubeproxy_addon" {
  count        = var.create_eks_kubeproxy_addon ? 1 : 0
  cluster_name = aws_eks_cluster.eks_cluster[0].name
  addon_name   = "kube-proxy"
  depends_on = [
    aws_eks_cluster.eks_cluster
  ]
}


resource "aws_eks_addon" "eks_ebscsi_addon" {
  count        = var.create_eks_ebscsi_addon ? 1 : 0
  cluster_name = aws_eks_cluster.eks_cluster[0].name
  addon_name   = "aws-ebs-csi-driver"
  depends_on = [
    aws_eks_cluster.eks_cluster
  ]
}