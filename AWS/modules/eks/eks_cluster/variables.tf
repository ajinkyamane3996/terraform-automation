variable "eks_cluster_creation" {
  type = bool
}

variable "name" {
  type = string
}

variable "role_arn" {
  type = string
}

variable "eks_version" {
  type = string
}

# variable "security_group_ids" {
#   type = list(string)
# }

variable "subnet_ids" {
  type = list(string)
}

variable "endpoint_private_access" {
  type = bool
}

variable "endpoint_public_access" {
  type = bool
}

variable "key_arn" {
  type = string
}

variable "resources_to_encrypt" {
  type = list(string)
}

variable "enabled_cluster_log_types" {
  type = list(string)
}

variable "retention_in_days" {
  type = number
}

variable "tags" {
  type = map(string)
}

variable "eks_cluster_tags" {
  type = map(string)
}

#addons
variable "create_eks_vpc_cni_addon" {
  type = bool
}

variable "create_eks_coredns_addon" {
  type = bool
}

variable "create_eks_kubeproxy_addon" {
  type = bool
}

variable "create_eks_ebscsi_addon" {
  type = bool
}