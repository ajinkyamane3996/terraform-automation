output "eks_arn" {
  value = aws_eks_cluster.eks_cluster[*].arn
}

output "eks_certificate_authority" {
  # value   = element(aws_eks_cluster.eks_cluster[*].certificate_authority[0], [""], 0)
  # value = aws_eks_cluster.eks_cluster[*].certificate_authority.data
  # value = [for i in aws_eks_cluster.eks_cluster : i.certificate_authority]
  # value = element(concat(aws_eks_cluster.eks_cluster[*].certificate_authority, [""]), 0)
  value       = element(concat(aws_eks_cluster.eks_cluster[*].certificate_authority[0].data, [""]), 0)
}

output "eks_id" {
  value = aws_eks_cluster.eks_cluster[*].id
}

output "eks_clsuter_endpoint" {
  value = aws_eks_cluster.eks_cluster[*].endpoint
}

output "eks_identity" {
  value = aws_eks_cluster.eks_cluster[*].identity
}

output "eks_platform_version" {
  value = aws_eks_cluster.eks_cluster[*].platform_version
}

output "name" {
  value = aws_eks_cluster.eks_cluster[*].name
}
