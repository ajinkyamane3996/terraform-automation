variable "create_eks_node_group" {
  type = bool
}



variable "cluster_name" {
  type = string
}

variable "launch_template_id" {
  type = string
}

variable "launch_template_version" {
  type = string
}



variable "node_role_arn" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}



variable "nodegroup_config" {
  type = list(object({
    name          = string
    capacity_type = string
    #subnet_ids           = list(string)
    force_update_version = bool
    desired_size         = number
    min_size             = number
    max_size             = number
    max_unavailable      = number
    eks_nodegroup_tags   = map(string)
    tags                 = map(string)
  }))
}