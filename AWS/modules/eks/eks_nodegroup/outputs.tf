output "eks_nodegroup_name" {
  value = aws_eks_node_group.eks_nodegroup[*].arn
}