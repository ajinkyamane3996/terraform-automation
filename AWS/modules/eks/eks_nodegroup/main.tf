resource "aws_eks_node_group" "eks_nodegroup" {
  count                = var.create_eks_node_group && length(var.nodegroup_config) > 0 ? length(var.nodegroup_config) : 0
  cluster_name         = var.cluster_name
  node_group_name      = var.nodegroup_config[count.index].name
  node_role_arn        = var.node_role_arn
  subnet_ids           = var.subnet_ids
  capacity_type        = var.nodegroup_config[count.index].capacity_type
  force_update_version = var.nodegroup_config[count.index].force_update_version


  scaling_config {
    desired_size = var.nodegroup_config[count.index].desired_size
    max_size     = var.nodegroup_config[count.index].max_size
    min_size     = var.nodegroup_config[count.index].min_size
  }

  update_config {
    max_unavailable = var.nodegroup_config[count.index].max_unavailable
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  launch_template {
    id      = var.launch_template_id
    version = var.launch_template_version
    # version = 1
  }

  tags = merge(
    var.nodegroup_config[count.index].tags,
    var.nodegroup_config[count.index].eks_nodegroup_tags
  )


}






