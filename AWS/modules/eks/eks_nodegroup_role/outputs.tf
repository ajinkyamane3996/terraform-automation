output "iam_role_name" {
  #value = aws_iam_role.cluster_role[*].name
  value = [for i in aws_iam_role.eks_nodegroup_role : i.name]
}

output "iam_role_arn" {
  value = aws_iam_role.eks_nodegroup_role[*].arn
  #value = [for i in aws_iam_role.cluster_role: i.arn]
}

output "iam_role_id" {
  #value = aws_iam_role.cluster_role[*].id
  value = [for i in aws_iam_role.eks_nodegroup_role : i.id]
}