output "launch_template_id" {
  description = "The ID of the Launch Template"
  value       = aws_launch_template.eks_launch_template[*].id
}


output "launch_template_name" {
  description = "The name of the Launch Template"
  value       = aws_launch_template.eks_launch_template[*].name
}

output "launch_template_version" {
  description = "The version of the Launch Template"
  value       = aws_launch_template.eks_launch_template[*].latest_version
}