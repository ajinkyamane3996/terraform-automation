variable "create_eks_launch_template" {
  type = bool
}

variable "name" {
  type = string
}

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string

}



variable "volume_size" {
  description = "The size of the EBS volume in GB"
  type        = number

}

variable "iops" {
  type = number
}

variable "volume_type" {
  type = string
}

variable "delete_on_termination" {
  type = bool
}

variable "encrypted" {
  description = "Whether to encrypt the EBS volume"
  type        = bool

}

variable "cluster_name" {
  type = string
}

variable "eks_cluster_endpoint" {
  type = string
}

variable "eks_certificate_authority" {
   type = string
}

variable "key_name" {
  type = string
}

variable "kms_key_id" {
  description = "The ID of the KMS key to use for EBS encryption"
  type        = string
  default     = "alias/aws/ebs"
}

variable "cluster_version" {
  type = string
}

