data "aws_ssm_parameter" "cluster" {
  name = "/aws/service/eks/optimized-ami/${var.cluster_version}/amazon-linux-2/recommended/image_id"
}


data "template_file" "backendapp_launch_template_userdata" {
  template = file("/home/ajinkya/Downloads/terraformv2-abhishek-v2/eks/eks_launch_template/templates/userdata.tpl")

  vars = {
    cluster_name        = var.cluster_name
    endpoint            = var.eks_cluster_endpoint
    cluster_auth_base64 = var.eks_certificate_authority

    bootstrap_extra_args = ""
    kubelet_extra_args   = ""
  }
}


resource "aws_launch_template" "eks_launch_template" {
  count                  = var.create_eks_launch_template ? 1 : 0
  name                   = var.name
  image_id               = data.aws_ssm_parameter.cluster.value
  instance_type          = var.instance_type
  key_name               = aws_key_pair.eks_key[0].key_name
  update_default_version = true

  # network_interfaces {
  #   device_index                = 0
  #   associate_public_ip_address = false
  #  # subnet_id                   = var.subnet_id
  # }

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
  }

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size           = var.volume_size
      iops                  = var.iops
      encrypted             = var.encrypted
      kms_key_id            = var.kms_key_id
      volume_type           = var.volume_type
      delete_on_termination = var.delete_on_termination
    }
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    }
  }

  user_data = base64encode(
     data.template_file.backendapp_launch_template_userdata.rendered,
   )
}

resource "tls_private_key" "pk" {
  count     = var.create_eks_launch_template ? 1 : 0
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "eks_key" {
  count      = var.create_eks_launch_template ? 1 : 0
  key_name   = var.key_name
  public_key = tls_private_key.pk[0].public_key_openssh

  provisioner "local-exec" {
    command = "echo '${tls_private_key.pk[0].private_key_pem}' > ./${var.key_name}.pem"
  }
}





