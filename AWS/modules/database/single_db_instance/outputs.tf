output "rds_instance_id" {
  value = aws_db_instance.single_db_instance[*].id
}
output "rds_instance_arn" {
  value = aws_db_instance.single_db_instance[*].arn
}
output "rds_instance_identifer" {
  value = aws_db_instance.single_db_instance[*].identifier
}