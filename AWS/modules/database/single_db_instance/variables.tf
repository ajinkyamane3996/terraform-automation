variable "db_instance_create_rds" {
  type = bool
}

variable "db_instance_allocated_storage" {
  type = number
}
variable "db_instance_max_allocated_storage" {
  type = number
}
variable "db_instance_storage_type" {
  type = string
}
variable "db_instance_engine" {
  type = string
}
variable "db_instance_engine_version" {
  type = number
}
variable "db_instance_instance_class" {
  type = string
}
variable "db_instance_identifier" {
  type = string
}
variable "db_instance_username" {
  type = string
}
variable "db_instance_password" {
  type = string
}
variable "db_instance_vpc_security_group_ids" {
  type = list(string)
}
variable "db_instance_maintenance_window" {
  type = string
}
variable "db_instance_backup_retention_period" {
  type = string
}
variable "db_instance_skip_final_snapshot" {
  type = bool
}
variable "db_instance_allow_major_version_upgrade" {
  type = bool
}
variable "db_instance_auto_minor_version_upgrade" {
  type = bool
}
variable "db_instance_apply_immediately" {
  type = bool
}
variable "db_instance_backup_window" {
  type = string
}

variable "db_instance_db_name" {
  type = string
}
variable "db_instance_db_subnet_group_name" {
  type = string
}
variable "db_instance_kms_key_id" {
  type = string
}
variable "db_instance_port" {
  type = string
}
variable "db_instance_multi_az" {
  type = bool
}
variable "db_instance_publicly_accessible" {
  type = bool
}

variable "db_instance_copy_tags_to_snapshot" {
  type = bool
}

variable "db_instance_delete_automated_backups" {
  type = bool
}
variable "db_instance_deletion_protection" {
  type = bool
}

variable "db_instance_monitoring_interval" {
  type = number
}

variable "db_instance_monitoring_role_arn" {
  type = string
}

variable "db_instance_parameter_group_name" {
  type = string
}

variable "db_instance_option_group_name" {
  type = string
}
variable "db_instance_performance_insights_enabled" {
  type = bool
}

variable "db_instance_performance_insights_kms_key_id" {
  type = string
}

variable "db_instance_performance_insights_retention_period" {
  type = number
}

variable "db_instance_storage_encrypted" {
  type = bool
}

#role
variable "db_instance_rds_monitoring_role_name" {
  type = string
}

variable "rds_monitoring_filename" {
  type = string
}


variable "single_db_instance_tags" {
  type = map(string)
}

variable "tags" {
  type = map(string)
}