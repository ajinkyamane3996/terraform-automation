resource "aws_db_instance" "single_db_instance" {
  count                                 = var.db_instance_create_rds ? 1 : 0
  allocated_storage                     = var.db_instance_allocated_storage
  max_allocated_storage                 = var.db_instance_max_allocated_storage
  storage_type                          = var.db_instance_storage_type
  engine                                = var.db_instance_engine
  engine_version                        = var.db_instance_engine_version
  instance_class                        = var.db_instance_instance_class
  identifier                            = var.db_instance_identifier
  username                              = var.db_instance_username
  password                              = var.db_instance_password
  vpc_security_group_ids                = var.db_instance_vpc_security_group_ids
  maintenance_window                    = var.db_instance_maintenance_window
  backup_retention_period               = var.db_instance_backup_retention_period
  skip_final_snapshot                   = var.db_instance_skip_final_snapshot
  allow_major_version_upgrade           = var.db_instance_allow_major_version_upgrade
  auto_minor_version_upgrade            = var.db_instance_auto_minor_version_upgrade
  apply_immediately                     = var.db_instance_apply_immediately
  backup_window                         = var.db_instance_backup_window
  publicly_accessible                   = var.db_instance_publicly_accessible
  multi_az                              = var.db_instance_multi_az
  db_name                               = var.db_instance_db_name
  db_subnet_group_name                  = var.db_instance_db_subnet_group_name
  kms_key_id                            = var.db_instance_kms_key_id
  port                                  = var.db_instance_port
  copy_tags_to_snapshot                 = var.db_instance_copy_tags_to_snapshot
  delete_automated_backups              = var.db_instance_delete_automated_backups
  deletion_protection                   = var.db_instance_deletion_protection
  monitoring_interval                   = var.db_instance_monitoring_interval
  monitoring_role_arn                   = var.db_instance_monitoring_role_arn != null ? aws_iam_role.rds_monitoring[0].arn : null
  parameter_group_name                  = var.db_instance_parameter_group_name
  option_group_name                     = var.db_instance_option_group_name
  performance_insights_enabled          = var.db_instance_performance_insights_enabled
  performance_insights_kms_key_id       = var.db_instance_performance_insights_kms_key_id
  performance_insights_retention_period = var.db_instance_performance_insights_retention_period
  storage_encrypted                     = var.db_instance_storage_encrypted
  tags = merge(
    var.tags,
    var.single_db_instance_tags
  )
}

resource "aws_iam_role" "rds_monitoring" {
  count              = var.db_instance_monitoring_interval != 0 ? 1 : 0
  name               = var.db_instance_rds_monitoring_role_name != "" ? var.db_instance_rds_monitoring_role_name : null
  assume_role_policy = file(var.rds_monitoring_filename)
}
resource "aws_iam_role_policy_attachment" "rds_monitoring" {
  count      = var.db_instance_monitoring_interval != 0 ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
  role       = aws_iam_role.rds_monitoring[0].name
}



