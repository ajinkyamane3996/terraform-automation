variable "replica_db_create" {
  type = bool
}
variable "replica_db_count" {
  type = number
}

variable "replica_db_replicate_source_db" {
  type = string
}
variable "replica_db_allocated_storage" {
  type = number
}
variable "replica_db_max_allocated_storage" {
  type = number
}
variable "replica_db_storage_type" {
  type = string
}
# variable "replica_db_engine" {
#   type = string
# }
# variable "replica_db_engine_version" {
#   type = number
# }
variable "replica_db_instance_class" {
  type = string
}
variable "replica_db_identifier" {
  type = string
}
# variable "replica_db_username" {
#   type = string
# }
# variable "replica_db_password" {
#   type = string
# }
variable "replica_db_vpc_security_group_ids" {
  type = list(string)
}
variable "replica_db_maintenance_window" {
  type = string
}
variable "replica_db_backup_retention_period" {
  type = string
}
variable "replica_db_skip_final_snapshot" {
  type = bool
}
variable "replica_db_allow_major_version_upgrade" {
  type = bool
}
variable "replica_db_auto_minor_version_upgrade" {
  type = bool
}
variable "replica_db_apply_immediately" {
  type = bool
}
variable "replica_db_backup_window" {
  type = string
}

# variable "replica_db_db_name" {
#   type = string
# }
variable "replica_db_db_subnet_group_name" {
  type = string
}
variable "replica_db_kms_key_id" {
  type = string
}
variable "replica_db_port" {
  type = string
}
variable "replica_db_multi_az" {
  type = bool
}
variable "replica_db_publicly_accessible" {
  type = bool
}

variable "replica_db_copy_tags_to_snapshot" {
  type = bool
}

variable "replica_db_delete_automated_backups" {
  type = bool
}
variable "replica_db_deletion_protection" {
  type = bool
}

variable "replica_db_monitoring_interval" {
  type = number
}

variable "replica_db_monitoring_role_arn" {
  type = string
}

variable "replica_db_monitoring_role_name" {
  type = string
}

variable "replica_db_monitoring_filename" {
  type = string
}

variable "replica_db_parameter_group_name" {
  type = string
}

variable "replica_db_option_group_name" {
  type = string
}
variable "replica_db_performance_insights_enabled" {
  type = bool
}

variable "replica_db_performance_insights_kms_key_id" {
  type = string
}

variable "replica_db_performance_insights_retention_period" {
  type = number
}

variable "replica_db_storage_encrypted" {
  type = bool
}

#role

variable "replica_db_tags" {
  type = map(string)
}

variable "tags" {
  type = map(string)
}