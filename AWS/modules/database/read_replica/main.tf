resource "aws_db_instance" "replica_db_instance" {
  count                                 = var.replica_db_create ? var.replica_db_count : 0
  replicate_source_db                   = var.replica_db_replicate_source_db
  allocated_storage                     = var.replica_db_allocated_storage
  max_allocated_storage                 = var.replica_db_max_allocated_storage
  storage_type                          = var.replica_db_storage_type
  instance_class                        = var.replica_db_instance_class
  identifier                            = var.replica_db_identifier
  vpc_security_group_ids                = var.replica_db_vpc_security_group_ids
  maintenance_window                    = var.replica_db_maintenance_window
  backup_retention_period               = var.replica_db_backup_retention_period
  skip_final_snapshot                   = var.replica_db_skip_final_snapshot
  allow_major_version_upgrade           = var.replica_db_allow_major_version_upgrade
  auto_minor_version_upgrade            = var.replica_db_auto_minor_version_upgrade
  apply_immediately                     = var.replica_db_apply_immediately
  backup_window                         = var.replica_db_backup_window
  publicly_accessible                   = var.replica_db_publicly_accessible
  multi_az                              = var.replica_db_multi_az
  db_subnet_group_name                  = var.replica_db_db_subnet_group_name
  kms_key_id                            = var.replica_db_kms_key_id
  port                                  = var.replica_db_port
  copy_tags_to_snapshot                 = var.replica_db_copy_tags_to_snapshot
  delete_automated_backups              = var.replica_db_delete_automated_backups
  deletion_protection                   = var.replica_db_deletion_protection
  monitoring_interval                   = var.replica_db_monitoring_interval
  monitoring_role_arn                   = var.replica_db_monitoring_role_arn != null ? aws_iam_role.rds_monitoring[0].arn : null
  parameter_group_name                  = var.replica_db_parameter_group_name
  option_group_name                     = var.replica_db_option_group_name
  performance_insights_enabled          = var.replica_db_performance_insights_enabled
  performance_insights_kms_key_id       = var.replica_db_performance_insights_kms_key_id
  performance_insights_retention_period = var.replica_db_performance_insights_retention_period
  storage_encrypted                     = var.replica_db_storage_encrypted
  tags = merge(
    var.tags,
    var.replica_db_tags
  )
}


resource "aws_iam_role" "rds_monitoring" {
  count              = var.replica_db_monitoring_interval != 0 ? 1 : 0
  name               = var.replica_db_monitoring_role_name != "" ? var.replica_db_monitoring_role_name : null
  assume_role_policy = file(var.replica_db_monitoring_filename)
}
resource "aws_iam_role_policy_attachment" "rds_monitoring" {
  count      = var.replica_db_monitoring_interval != 0 ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
  role       = aws_iam_role.rds_monitoring[0].name
}