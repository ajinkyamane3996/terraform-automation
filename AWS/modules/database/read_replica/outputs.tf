output "replica_instance_id" {
  value = aws_db_instance.replica_db_instance[*].id
}
output "replica_instance_arn" {
  value = aws_db_instance.replica_db_instance[*].arn
}
output "replica_instance_identifer" {
  value = aws_db_instance.replica_db_instance[*].identifier
}