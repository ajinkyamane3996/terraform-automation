resource "aws_db_parameter_group" "this" {
  count = var.create_parameter_group ? 1 : 0

  name   = var.name
  family = var.family

  dynamic "parameter" {
    for_each = var.parameters
    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }

  tags = merge(
    var.tags,
    var.db_parameter_group_tags
  )
}

