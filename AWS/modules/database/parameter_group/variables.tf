variable "create_parameter_group" {
  type = bool
}

variable "name" {
  type = string

}

variable "family" {
  type = string

}

variable "parameters" {
  type = map(object({
    name  = string
    value = string
  }))

}

variable "tags" {
  type = map(string)
}

variable "db_parameter_group_tags" {
  type = map(string)
}
