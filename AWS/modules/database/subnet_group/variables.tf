variable "create_db_subnet_group" {
  type = bool
}

variable "name" {
  type = string
}

variable "description" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "tags" {
  type = map(string)
}

variable "db_subnet_group_tags" {
  type = map(string)
}