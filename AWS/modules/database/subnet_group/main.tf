resource "aws_db_subnet_group" "subnet_group" {
  count       = var.create_db_subnet_group ? 1 : 0
  name        = var.name
  description = var.description
  subnet_ids  = var.subnet_ids
  tags = merge(
    var.tags,
    var.db_subnet_group_tags
  )
}