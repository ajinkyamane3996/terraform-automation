# variables.tf
variable "option_group_name" {
  description = "The name of the option group."
  type        = string
}

variable "create_db_option_group" {
  type = bool
}

variable "engine_name" {
  description = "The name of the database engine that this option group should be associated with."
  type        = string
}

variable "major_engine_version" {
  description = "The major version of the database engine that this option group should be associated with."
  type        = string
}

variable "option_group_description" {
  description = "The description of the option group."
  type        = string
}

variable "option_group_options" {
  description = "A list of option settings to apply to the option group."
  type = list(object({
    option_name = string
    option_settings = list(object({
      name  = string
      value = string
    }))
  }))
}

variable "tags" {
  type = map(string)
}

variable "db_option_group_tags" {
  type = map(string)
}