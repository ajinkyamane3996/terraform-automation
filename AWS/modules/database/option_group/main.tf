resource "aws_db_option_group" "option_group" {
  count                    = var.create_db_option_group ? 1 : 0
  name                     = var.option_group_name
  engine_name              = var.engine_name
  major_engine_version     = var.major_engine_version
  option_group_description = var.option_group_description

  dynamic "option" {
    for_each = var.option_group_options
    content {
      option_name = option.value.option_name

      dynamic "option_settings" {
        for_each = option.value.option_settings
        content {
          name  = option_settings.value.name
          value = option_settings.value.value
        }
      }
    }
  }
  tags = merge(
    var.tags,
    var.db_option_group_tags
  )
}