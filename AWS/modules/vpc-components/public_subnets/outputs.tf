output "public_subnets" {
  value = aws_subnet.public_subnets[*].id
}

output "public_subnet_ids" {
  value = [for subnet in aws_subnet.public_subnets: subnet.id]
}
