variable "kms_key_ids" { 
  type = list(string)  
  default = []
} 

variable "kms_key_arn" {
  type = list(string)  
  default = []
}

variable "sqs_queue_config" {
  type = list(object({
    sqs_queue_name = string
    max_message_size = number
    message_retention_seconds= number
    receive_wait_time_seconds= number
    delay_seconds= number
    kms_data_key_reuse_period_seconds= number
    # tags= []
  }))
  default = [ ]
}


