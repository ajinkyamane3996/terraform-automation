## Create an SQS queue and encrypt it using the KMS key
resource "aws_sqs_queue" "example" {
  count = length(var.sqs_queue_config)
  name = var.sqs_queue_config[count.index].sqs_queue_name
  max_message_size          = var.sqs_queue_config[count.index].max_message_size
  message_retention_seconds = var.sqs_queue_config[count.index].message_retention_seconds
  receive_wait_time_seconds = var.sqs_queue_config[count.index].receive_wait_time_seconds  
  delay_seconds             = var.sqs_queue_config[count.index].delay_seconds
  kms_master_key_id         = var.kms_key_ids[2]
  # kms_master_key_id       = var.kms_key_arn[2]
  kms_data_key_reuse_period_seconds = var.sqs_queue_config[count.index].kms_data_key_reuse_period_seconds

}


# resource "aws_sqs_queue" "terraform_queue" {
#   name                      = "terraform-example-queue"
#   delay_seconds             = 90
#   max_message_size          = 2048
#   message_retention_seconds = 86400
#   receive_wait_time_seconds = 10
#   redrive_policy = jsonencode({
#     deadLetterTargetArn = aws_sqs_queue.terraform_queue_deadletter.arn
#     maxReceiveCount     = 4
#   })

#   tags = {
#     Environment = "production"
#   }
# }

